import React, { useCallback } from "react";
import { TextInput, TextInputProps } from "./text-input";

interface StringFilterProps extends TextInputProps {
  matchList?: string[];
  onChange?(value: string | undefined): void;
  onComplete?(value: string | undefined): void;
}

export function StringFilter(props: StringFilterProps) {
  const { onChange } = props;

  return (
    <TextInput
      {...props}
      className={props.className}
      debounceDelay={300}
      iconName={"search"}
      placeholder={props.placeholder}
      type="text"
      onChange={onChange}
    />
  );
}
