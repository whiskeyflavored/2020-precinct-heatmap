import React from "react";
import cx from "classnames";
import styles from "./load-indicator.module.scss";

interface LoadIndicatorProps {
  className?: string;
}

export function LoadIndicator({
  className,
}: LoadIndicatorProps): React.ReactElement {
  return (
    <div className={cx(className, styles.wrapper)}>
      <div className={cx(styles.bubble, styles.A)}></div>
      <div className={cx(styles.bubble, styles.B)}></div>
      <div className={cx(styles.bubble, styles.C)}></div>
      <div className={cx(styles.bubble, styles.D)}></div>
      <div className={cx(styles.bubble, styles.E)}></div>
    </div>
  );
}
