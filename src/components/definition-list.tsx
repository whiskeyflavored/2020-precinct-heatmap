import React from "react";
import styles from "./definition-list.module.scss";

export interface DefinitionListProps {
  children?: NodeList | React.ReactNode | React.ReactElement[];
  dictionary?: any;
}

export function DefinitionList({ children }: DefinitionListProps) {
  return <dl className={styles["definition-list"]}>{children}</dl>;
}
