import React, { useState, useCallback, useEffect, useRef } from "react";
import { Icon } from "ts-react-feather-icons";
import styles from "./search-bar.module.scss";
import cx from "classnames";
import { searchForPrecintsInState } from "../etc/api";
import { matchParent } from "../etc/DOM-helpers";

interface SearchBarProps {
  controlledValue?: string;
  debounceDelay?: number;
  onComplete?(geo_id: string | undefined): void;
}

export function SearchBar({
  controlledValue,
  debounceDelay = 300,
  onComplete,
}: SearchBarProps): React.ReactElement {
  //
  const [value, setValue] = useState<string | undefined>(controlledValue);

  const [isFetching, setFetching] = useState<boolean>(false);

  const [queryMatch, setQueryMatch] = useState<string | undefined>();

  const [queryResults, setQueryResults] = useState<any>();

  const clearBtnRef: React.RefObject<HTMLButtonElement> = useRef(null);

  const inputRef: React.RefObject<HTMLInputElement> = useRef(null);

  const hasValue: boolean = Boolean(inputRef.current?.value);

  const containerCx: string = cx(styles["search-component"], {
    [styles["expanded"]]: Boolean(queryResults),
  });

  const headerCx: string = cx(styles["search-bar-wrapper"], {
    [styles["empty"]]: !hasValue,
    [styles["loading"]]: isFetching,
  });

  const iconName: any = isFetching ? "loader" : hasValue ? "x" : "search";

  const clearInput = useCallback(() => {
    if (inputRef.current && clearBtnRef.current) {
      inputRef.current.value = "";

      clearBtnRef.current.blur();

      setValue(undefined);

      setFetching(false);

      setQueryMatch(undefined);

      setQueryResults(undefined);

      if (onComplete) {
        onComplete(undefined);
      }
    }
  }, [clearBtnRef, inputRef, onComplete]);

  const handleValueChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const target: HTMLInputElement = event.target as HTMLInputElement;

      target.value ? setValue(target.value) : clearInput();
    },
    [setValue, clearInput]
  );

  const debounceCallback = useCallback(async () => {
    if (value && inputRef.current?.value === value) {
      setQueryResults(await searchForPrecintsInState("GA", value));

      setQueryMatch(value);

      setFetching(false);
    }
  }, [inputRef, value]);

  const handleSelectOption = useCallback(
    (event: React.MouseEvent<HTMLLIElement>) => {
      const target: HTMLLIElement = event.target as HTMLLIElement;

      const rootTarget: HTMLInputElement | undefined = matchParent(
        target,
        (currentTarget: HTMLElement) =>
          currentTarget.classList.contains(styles["precinct-entry"])
      ) as HTMLInputElement;

      const { geo_id } = rootTarget.dataset;

      if (geo_id) {
        setQueryMatch(undefined);

        setQueryResults(undefined);

        if (inputRef.current) {
          inputRef.current.value = geo_id || "";
        }

        if (onComplete) {
          onComplete(geo_id);
        }
      }
    },
    [inputRef, onComplete]
  );

  const renderOptionList = useCallback(() => {
    if (value && value === queryMatch) {
      const optionElements: React.ReactElement[] = [];

      for (let [precinctName, countyName, geo_id] of queryResults) {
        // get the index offset of the match position in the precinct name
        const matchIndex: number = precinctName
          .toLowerCase()
          .indexOf(value.toLowerCase());

        // grab the actual case-sensitive matching string fragment
        const highlightString: string = precinctName.slice(
          matchIndex,
          matchIndex + value.length
        );

        // split the precinct name into parts. this allows us to
        // compose the precinct name out of spans, of which we can
        // style appropriately absed on matching value
        const spans: any[] = precinctName.split(highlightString);

        // the initial string fragment, conditional on match position
        const valueChunkA: string =
          spans.length === 1 && matchIndex === 0 ? highlightString : spans[0];

        // the 'middle' (or ending) fragment, conditional on match position
        const valueChunkB: string =
          valueChunkA === highlightString ? spans[0] : highlightString;

        // if the string isnt matched at the start or end of the string,
        // add the remaining fragment to complete the precinct name
        const valueChunkC: string | undefined =
          spans.length > 1 ? spans[1] : undefined;

        // compose the list element into our options list
        optionElements.push(
          <li
            className={styles["precinct-entry"]}
            key={`${precinctName}-${countyName}`}
            data-locality-id={geo_id}
            data-precinct-name={precinctName}
            data-county-name={countyName}
            onClick={handleSelectOption}
          >
            <div className={styles["precinct-name"]}>
              <span>{valueChunkA}</span>
              <span className={styles["highlight"]}>{valueChunkB}</span>
              {valueChunkC ? <span>{valueChunkC}</span> : undefined}
            </div>
            <div className={styles["county-name"]}>
              <span>{countyName} county</span>
            </div>
          </li>
        );
      }

      return optionElements;
    }
  }, [value, queryMatch, queryResults, handleSelectOption]);

  useEffect(() => {
    if (value) {
      setFetching(true);

      setTimeout(debounceCallback, debounceDelay);
    }
  }, [value, debounceDelay, debounceCallback]);

  useEffect(() => {
    if (inputRef.current) {
      controlledValue
        ? (inputRef.current.value = controlledValue)
        : clearInput();
    }
  }, [controlledValue, inputRef, clearInput]);

  //
  return (
    <div className={containerCx}>
      <header className={headerCx}>
        <input
          tabIndex={0}
          ref={inputRef}
          type="text"
          placeholder="Find a precinct"
          className={styles["search-input"]}
          onChange={handleValueChange}
        ></input>
        <button
          ref={clearBtnRef}
          tabIndex={hasValue ? 0 : -1}
          className={styles.action}
          onClick={clearInput}
        >
          <Icon name={iconName} size={20} />
        </button>
      </header>
      <section className={styles["option-list-window"]}>
        <ul>{renderOptionList()}</ul>
      </section>
    </div>
  );
}
