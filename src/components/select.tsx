import React, { useCallback, useState } from "react";
import cx from "classnames";
import styles from "./select.module.scss";

interface SelectProps {
  className?: string;
  children?:
    | JSX.Element
    | React.ReactElement[]
    | React.ReactElement[]
    | React.ReactChildren
    | React.ReactNode;
  defaultValue?: string | number;
  disabled?: boolean;
  multiple?: boolean;
  value?: string | number;
  onChange?(value: string | number | string[] | undefined): void;
}

export function Select({
  className,
  children,
  defaultValue,
  disabled = false,
  multiple = false,
  value,
  onChange,
}: SelectProps) {
  // component stateful values
  const [_value, setValue] = useState<string | number | undefined>(
    defaultValue || value
  );

  // combine classnames if they are supplied
  const selectCx: string = cx(styles["select-input"], {
    className: Boolean(className),
  });

  // triggered when a user changes the <select> elemenet value
  const handleSelectValueChange = useCallback(
    (event: React.ChangeEvent) => {
      // coerce this to expect a <select> element as the expected target element type
      const target: HTMLSelectElement = event.target as HTMLSelectElement;

      if (target) {
        // assign target value to component state if target is defined
        setValue(target.value);

        // call the handler to change app-level stateAbbr based on <option> value
        if (onChange) {
          onChange(target.value);
        }
      }
    },
    [onChange]
  );

  return (
    <select
      className={selectCx}
      defaultValue={defaultValue}
      disabled={disabled}
      multiple={multiple}
      value={value || _value}
      onChange={handleSelectValueChange}
    >
      {children}
    </select>
  );
}
