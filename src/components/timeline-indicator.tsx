import React, { useContext } from "react";
import { timelineSizes } from "../etc/constants";
import { TimestampContext } from "../stores/timeline.store";
import styles from "./timeline-indicator.module.scss";

// Indocator overlain ontop of the timeline acting as a marker of the active
// timestamp present within the timeline. The indicator reflexively positions itself
// to the proper location in the timeline, and is also interactive to allow
// dragging/snapping to timeline marker positions. These controls are managed
// at the parent level.
export function TimelineIndicator() {
	// measurements needed for rendering extracted from constants file
  const { overscrollThreshold, rulerHeight, trackHeight } = timelineSizes;

	// Reference the latest position of the current timestamp marker
  const { currentTimestampPositionX } = useContext(TimestampContext);

	// derive the width to use to render the indicator
  const markerWidth: number = rulerHeight;

	// a separate width used for the marker background
  const backgroundMarkerWidth: number = (rulerHeight * 3) / 2.5;

	// A delta value used in rendering calculations
  const markerDeltaX: number = backgroundMarkerWidth - markerWidth;

	// the position to be placed on the timeline
  const offsetX: number =
    (currentTimestampPositionX || -overscrollThreshold) - markerWidth / 2;

	// Starting Y position to apply during rendering
  const y1: number = 0;

	// Ending Y position to apply during rendering
  const y2: number = rulerHeight * 2 + trackHeight;

	// Render the SVG component
  return (
    <svg x={offsetX} className={styles["timeline-indicator"]}>
      <rect width={markerWidth} height={y2 - y1} y={y1}></rect>
      <path
        className={styles["shadow"]}
        d={`M ${-markerDeltaX} 0 l ${
          backgroundMarkerWidth + markerDeltaX
        } 0 l -${(backgroundMarkerWidth + markerDeltaX) / 2} ${
          (backgroundMarkerWidth + markerDeltaX) / 2
        } z`}
      ></path>
      <line x1={markerWidth / 2} x2={markerWidth / 2} y1={y1} y2={y2}></line>
      <path
        className={styles["marker"]}
        d={`M 0 0 l ${markerWidth} 0 l -${markerWidth / 2} ${
          markerWidth / 2
        } z`}
      ></path>
    </svg>
  );
}
