import React, { useContext, useEffect, useRef } from "react";
import { MapPopoverContext } from "../stores/map.store";
import styles from "./map-popover.module.scss";
import cx from "classnames";

export function MapPopover() {
  const popoverElement = useRef<HTMLDivElement>(null);

  const {
    isMapPopoverVisible,
    mapPopoverCountyName,
    mapPopoverPositionX,
    mapPopoverPositionY,
    mapPopoverPrecinctName,
  } = useContext(MapPopoverContext);

  const popoverCx: string = cx(styles["map-popover"], {
    [styles.hide]: !isMapPopoverVisible,
  });

  useEffect(
    function updatePosition() {
      if (popoverElement.current) {
        popoverElement.current.style.left = `${mapPopoverPositionX}px`;
        popoverElement.current.style.top = `${mapPopoverPositionY}px`;
      }
    },
    [mapPopoverPositionX, mapPopoverPositionY, popoverElement]
  );

  return (
    <div ref={popoverElement} className={popoverCx}>
      <span className={styles["precinct-name"]}>{mapPopoverPrecinctName}</span>
      <span className={styles["county-name"]}>
        {mapPopoverCountyName} county
      </span>
    </div>
  );
}
