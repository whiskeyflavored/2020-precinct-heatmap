import React, { CSSProperties, useEffect, useRef, useState } from "react";
import cx from "classnames";
import styles from "./ratio-bar.module.scss";

export interface RatioBarProps {
  partyName?: string;
  votes: number;
  totalVotes: number;
}

export function RatioBar({ partyName, votes, totalVotes }: RatioBarProps) {
  // Reference the top-level wrapper element to the rendered component
  const barRef: React.RefObject<HTMLDivElement> = useRef(null);

  // Store the width of the bar fill in component state
  const [barFillWidth, setBarFillWidth] = useState<number>(0);

  // Derive the numeric value to be displayed in the ratio bar
  const ratio: number = calculateVoteRatio(votes, totalVotes);

  // Transform the ratio into a human readable string to use as a label
  const percentLabelValue: string = (ratio * 100).toFixed(0);

  // Derive styles to be applied to the fill bar
  const barStyles: CSSProperties = {
    width: `${barFillWidth}px`,
  };

  // Derive classnames to be applied to the fill bar to color them based on
  // component props / partyValue
  const cxName: string = cx(styles["percent-majority-votes"], {
    [styles["dem"]]: partyName === "dem",
    [styles["gop"]]: partyName === "gop",
  });

  // Derive where the label should be rendered, either inside or outside of the fill bar
  const labelCx: string = cx({
    [styles["outside"]]: ratio < 0.33,
  });

  // when the prop values change for votes & totalVotes, update the bar width
  useEffect(() => {
    if (barRef.current) {
      const bounds: ClientRect = barRef.current.getBoundingClientRect();

      setBarFillWidth(ratio * bounds.width);
    }
  }, [barRef, ratio, votes, totalVotes]);

  // render the component
  return (
    <div className={styles["ratio-bar"]} ref={barRef}>
      <div className={cxName} style={barStyles}>
        <label className={labelCx}>
          <strong>{percentLabelValue}%</strong>
        </label>
      </div>
    </div>
  );
}

function calculateVoteRatio(majorityVotes: number, totalVotes: number): number {
  return totalVotes ? majorityVotes / totalVotes : 0;
}
