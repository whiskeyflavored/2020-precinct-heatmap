import React, { useState, useCallback, useEffect, useRef } from "react";
import { Icon, IconName } from "ts-react-feather-icons";
import styles from "./text-input.module.scss";
import cx from "classnames";

export interface TextInputProps {
  className?: string;
  controlledValue?: string;
  debounceDelay?: number;
  iconName?: string;
  iconSize?: number;
  placeholder?: string;
  type?: string;
  onChange?(value: string | undefined): void;
  onComplete?(geo_id: string | undefined): void;
}

export function TextInput({
  className,
  controlledValue,
  debounceDelay = 0,
  iconName,
  iconSize = 20,
  placeholder,
  type = "text",
  onChange,
  onComplete,
}: TextInputProps): React.ReactElement {
  // component state variables
  const [value, setValue] = useState<string | undefined>(controlledValue);

  const [isFetching, setFetching] = useState<boolean>(false);

  const clearBtnRef: React.RefObject<HTMLButtonElement> = useRef(null);

  const inputRef: React.RefObject<HTMLInputElement> = useRef(null);

  const hasValue: boolean = Boolean(inputRef.current?.value);

  // container classname composition
  const containerCx: string = cx(styles["search-component"], {
    [`${className}`]: Boolean(className),
  });

  // determine statefulness of which icon to display, depending on component state
  const useIcon: string | undefined = isFetching
    ? "loader"
    : hasValue
    ? "x"
    : iconName;

  // an invokable method which clears the current state of the component
  // value and state.
  const clearInput = useCallback(() => {
    if (inputRef.current && clearBtnRef.current) {
      inputRef.current.value = "";

      clearBtnRef.current.blur();

      setValue(undefined);

      setFetching(false);

      if (onChange) {
        onChange(undefined);
      }

      if (onComplete) {
        onComplete(undefined);
      }
    }
  }, [clearBtnRef, inputRef, onChange, onComplete]);

  // Component-level handler to manage internal element state on new
  // values. Updates compoment state only.
  const handleValueChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const target: HTMLInputElement = event.target as HTMLInputElement;

      target.value ? setValue(target.value) : clearInput();
    },
    [setValue, clearInput]
  );

  // After the debounceDelay time has been reached, trigger any value change
  // handlers if the compoment value is in sync with the input element's value.
  const debounceCallback = useCallback(async () => {
    if (onChange && value && inputRef.current?.value === value) {
      onChange(value);

      setFetching(false);
    }
  }, [inputRef, value, onChange]);

  // When the value of the input changes, and given that there is a
  // onChange handler, debounce any handling of that change.
  useEffect(() => {
    if (value && onChange) {
      setFetching(true);

      setTimeout(debounceCallback, debounceDelay);
    }
  }, [value, debounceDelay, debounceCallback, onChange]);

  // When the controlledValue property changes, detect that change and
  // set the component value to the property value. If the value is
  // undefined, clear the input state
  useEffect(() => {
    if (inputRef.current) {
      controlledValue
        ? (inputRef.current.value = controlledValue)
        : clearInput();
    }
  }, [controlledValue, inputRef, clearInput]);

  //
  return (
    <div className={containerCx}>
      <input
        tabIndex={0}
        type={type}
        ref={inputRef}
        placeholder={placeholder}
        className={styles["search-input"]}
        onChange={handleValueChange}
      ></input>
      {useIcon ? (
        <button
          ref={clearBtnRef}
          tabIndex={hasValue ? 0 : -1}
          className={styles.action}
          onClick={clearInput}
        >
          <Icon name={useIcon as IconName} size={iconSize} />
        </button>
      ) : null}
    </div>
  );
}
