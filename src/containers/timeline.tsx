import React, {
  useCallback,
  useContext,
  useLayoutEffect,
  useEffect,
  useMemo,
  useRef,
} from "react";
import cx from "classnames";
import styles from "./timeline.module.scss";

// import components
import { TimelineIndicator } from "../components/timeline-indicator";
import { TimelineMarkers } from "./timeline-markers";
import { TimelineRuler } from "./timeline-ruler";

// import helper methods
import {
  centerViewboxOnTimestampMarker,
  panTimeline,
} from "../etc/timeline-helpers";
import { timelineSizes } from "../etc/constants";
import { convertTimestampToLocalTime } from "../etc/leaflet-helpers";

// import store contexts
import { ActionType, DispatchActions } from "../stores/types";
import {
  TimelineContext,
  TimestampContext,
  TimelineActionsContext,
  TimestampActionsContext,
} from "../stores/timeline.store";

// define the shape of the Timeline compoent props
export interface TimelineProps {
  children?: React.ReactNode;
  className?: string;
  disable?: boolean;
}

// expose our component as a module
export function Timeline({ className, disable = true }: TimelineProps) {
  // destructure timeline context level state
  const {
    focusTransition,
    timelineEndTime,
    timelineHasInteraction,
    timelineInteractionInitialX,
    timelinePanX,
    timelineRuleCount,
    timelineScale,
    timelineStartTime,
    timelineWidth,
  } = useContext(TimelineContext);

  // store values of current timestamp info
  const {
    currentTimestamp,
    currentTimestampPositionX,
    timestampsForPrecinct,
    timestampsForState,
  } = useContext(TimestampContext);

  // destructure reference to timeline context dispatcher
  const timelineActionsDispatch = useContext(TimelineActionsContext);

  // Reference to timestamp context dispatcher
  const timestampActionsDispatch = useContext(TimestampActionsContext);

  // Reference to the indicator element within the timeline
  const indicatorRef: React.RefObject<SVGGElement> = useRef(null);

  const indicatorHasInteraction: React.MutableRefObject<boolean> = useRef(
    false
  );

  // React element refs
  const svgRef: React.RefObject<SVGSVGElement> = useRef(null);

  // Build style classnames based on props, currentStateAbbr, and a base class
  const containerCx: string = cx(className, styles["timeline-wrapper"], {
    [styles["disabled"]]: disable,
  });

  // how much additional space to allow the svg area to scroll horizontally
  const { overscrollThreshold } = timelineSizes;

  // Construct a dictionary of all timestamps and their offsets in relation to the
  // timeline for a given state. This dictionary is kept in component state.
  // Timestamp rendering depends on this data for accurate placement on the timeline.
  // The dictionary is keyed by the position offset within the timeline, and is updated
  // if any dependent variables to that derived value are changed, such as zoom factor.
  const timestampDictionary = useMemo(
    function () {
      // Reference the constant value of the width of a time unit (1 hour)
      // used to calculate marker position in reference to the timeline
      const { timeUnitSize } = timelineSizes;

      if (timestampsForState && timelineRuleCount) {
        // hold the data object for our new timestamp dictionary
        const dictionary: any = {};

        for (let timestamp of timestampsForState) {
          // Create a date instance from the current timestamp
          const timestampDate: Date = convertTimestampToLocalTime(timestamp);

          // each timestamp must take into account any offsetting timezone shift relative
          // to the initial timeline start time to account for DST changes over time.
          const daylightSavingsDeltaInHours: number =
            (timelineStartTime.getTimezoneOffset() -
              timestampDate.getTimezoneOffset()) /
            60;

          // calculate the coefficinet of the relative scale of the furrent timestamp
          // in context of the start and end point of the timeline
          const deltaXCoeff: number =
            (timestampDate.getTime() - timelineStartTime.getTime()) /
            (timelineEndTime.getTime() - timelineStartTime.getTime());

          // calculate the offset to render the marker on the timeline.
          // calculates first a ratio of the current timestamp relative to the
          // start and end timestamps.
          const markerPositionX: number =
            deltaXCoeff *
            ((timelineRuleCount + daylightSavingsDeltaInHours) *
              timeUnitSize *
              timelineScale);

          // update the marker positions array with information on this marker
          dictionary[`${markerPositionX}`] = {
            timestamp,
          };
        }

        return dictionary;
      }
    },
    [
      timelineEndTime,
      timelineRuleCount,
      timelineScale,
      timelineStartTime,
      timestampsForState,
    ]
  );

  // calculate and memoize the scaled width of the timeline. This is dependent on
  // a stateful value of the calculated timeline width (dynamically determined by state)
  // and multiplied by the zoom-factor of the timeline
  const scaledTimelineWidth: number = useMemo(
    () => timelineWidth * timelineScale,
    [timelineScale, timelineWidth]
  );

  // calculate track width with built-in overscroll buffer for the rendered timeline
  const trackWidth: number = useMemo(
    () =>
      scaledTimelineWidth ? scaledTimelineWidth + overscrollThreshold * 2 : 0,
    [scaledTimelineWidth, overscrollThreshold]
  );

  // a local dict to conveniently reference positions based on a timestamp key for
  // all precinct-level timestamps for this US state.
  const precinctMarkerDictionary: any = useMemo(
    function () {
      const dictionary: any = {};

      for (let positionKey in timestampDictionary) {
        const { timestamp } = timestampDictionary[positionKey];

        // compare the state timestamp in the dict to see if it is included in the
        // precinct level data. if so, mark this in the local dictionary.
        if (timestampsForPrecinct?.includes(timestamp)) {
          dictionary[positionKey] = timestamp;
        }
      }

      return dictionary;
    },
    [timestampDictionary, timestampsForPrecinct]
  );

  // Mouse and Touch events have different means of reporting the current
  // interaction position. This function extracts that information in an agnostic
  // way, and returns an object containing the position value for either event type.
  const extractClientXFromInteractionEvent = useCallback(function (
    event: MouseEvent | TouchEvent
  ): { clientX: number | undefined } {
    // destructure touches TouchList from the event as coerced into a TouchEvent
    const { touches } = event as TouchEvent;

    // Conditionally extract the clientX offset from either the TouchList first touch
    // or as a fallback, assume mouse interaction.
    const { clientX } = touches?.length ? touches[0] : (event as MouseEvent);

    return { clientX };
  },
  []);

  // Update an interaction flag when the indicator has interaction
  const handleIndicatorMouseDown = useCallback(
    function () {
      indicatorHasInteraction.current = true;
    },
    [indicatorHasInteraction]
  );

  // Handler for initial interaction with the timeline. Sets the store to recognize a flag
  // an interaction has occurred, and captures initial offset touchpoints in the viewport.
  const handleTimelineInteractionStart = useCallback(
    (event: React.MouseEvent | React.TouchEvent) => {
      const { clientX } = extractClientXFromInteractionEvent(event.nativeEvent);

      if (svgRef.current && clientX) {
        // Update the store to indicate an interaction has taken place, and other information
        // about the initial interaction offset, and current timeline pan offset.
        timelineActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            timelineHasInteraction: true,
            timelineInteractionInitialX: clientX,
            // instantiate the viewBox width to a value if none currently exists.
            // this prevents future 'undefined' error cases
            timelinePanX: svgRef.current.viewBox.baseVal?.x || 0,
          },
        } as ActionType.UpdateTimeline);
      }
    },
    [extractClientXFromInteractionEvent, timelineActionsDispatch, svgRef]
  );

  // Allow mousewheel to manage scroll state of the timeline
  const handleMouseWheel = useCallback(
    (event: WheelEvent) => {
      if (svgRef.current) {
        // stop default behavior from browser. This may trigger navigation or
        // bounce-back behavior that is undesirable
        event.preventDefault();

        // stop further bubbling of the event to prevent undesirable behavior
        event.stopPropagation();

        // extract current offset value of the viewBox
        const currentPanX: number = svgRef.current.viewBox.baseVal?.x || 0;

        // Extract deltas from the mousewheel event. We capture both X and Y deltas
        // to support both traditional mousewheels in addition to trackpad/3d mouse
        const { deltaX, deltaY } = event;

        // calculate viewbox offset value
        const calcViewboxOffsetX: number =
          currentPanX + (deltaX || deltaY) / timelineScale;

        // invoke the actual assignment of offset value to the base SVG element
        panTimeline(calcViewboxOffsetX, scaledTimelineWidth, svgRef);
      }
    },
    [svgRef, timelineScale, scaledTimelineWidth]
  );

  const handlePanTimeline = useCallback(
    function (event: MouseEvent | TouchEvent) {
      // Derive the offset from the interaction at this invokation
      const { clientX } = extractClientXFromInteractionEvent(event);

      // Conditionally proceed only if a valid offset value was returned
      if (clientX) {
        // calculate what the new viewbox offset should be. this value scrolls the view area.
        const calcViewboxOffsetX: number =
          timelinePanX + timelineInteractionInitialX - clientX;

        // invoke method to pan the timeline
        panTimeline(calcViewboxOffsetX, scaledTimelineWidth, svgRef);
      }
    },
    [
      extractClientXFromInteractionEvent,
      scaledTimelineWidth,
      svgRef,
      timelineInteractionInitialX,
      timelinePanX,
    ]
  );

  // Handler for any draggable interaction to navigate the timeline via horizontal drag.
  // Pans the timeline based on current viewbox offsets and touch coordinates.
  const handleMouseMove = useCallback(
    (event: MouseEvent | TouchEvent) => {
      // Extract the interaction offset from the client viewport origin of the x dimension
      const { clientX = 0 } = extractClientXFromInteractionEvent(event);

      const { left = 0 } = svgRef?.current?.getBoundingClientRect() || {};

      // Conditionally branch on how to proceed given 2 scenarios
      switch (true) {
        // Scenario A:
        // Drag the marker and snap to timestamp markers
        case indicatorHasInteraction.current:
          // Derive where the mouse position is relative to the timeline
          const interactionPositionX: number = Math.round(
            clientX + timelinePanX - left
          );

          let snapPositionX: number = 0;

          const useDictionary: any = Object.keys(precinctMarkerDictionary)
            .length
            ? precinctMarkerDictionary
            : timestampDictionary;

          const markerMapKeys: string[] = Object.keys(useDictionary);

          // Iterate through the dictionary keys of markers on the timeline
          markerMapKeys.some(function (key: string, index: number): boolean {
            // Extract the floating point number of the offset
            const thisMarkerPosition: number = parseFloat(key);

            // Grab reference to the next sibling marker
            const nextMarkerPosition: any = parseFloat(
              markerMapKeys[index + 1]
            );

            // Have not reached the end of the marker dictionary
            if (!isNaN(nextMarkerPosition)) {
              // detects if the mouse is outside the left boundary of
              // the marker range within the timeline.
              if (interactionPositionX < thisMarkerPosition && !index) {
                snapPositionX = thisMarkerPosition;

                return true;
              }
              // Determine between which 2 markers the marker position falls
              else if (
                interactionPositionX >= thisMarkerPosition &&
                interactionPositionX <= nextMarkerPosition
              ) {
                // Derive which marker is closer to the interaction coordinates
                const closestMarkerPosition: number =
                  (interactionPositionX - thisMarkerPosition) /
                    (nextMarkerPosition - thisMarkerPosition) <
                  0.5
                    ? thisMarkerPosition
                    : nextMarkerPosition;

                // Set the snap position to the closer marker
                snapPositionX = closestMarkerPosition;

                return true;
              } else {
                return false;
              }
            } else {
              // Have reached the end of the marker dictionary.
              // Set the snap position to the last marker (current)
              snapPositionX = parseFloat(key);

              return true;
            }
          });

          // Conditionally update the store to reflect the marker's new position
          // if a marker exists on the marker map at this offset.
          if (snapPositionX) {
            timestampActionsDispatch({
              type: DispatchActions.updateTimeline,
              details: {
                currentTimestampPositionX: snapPositionX,
              },
            } as ActionType.UpdateTimeline);
          }

          break;

        // Scenario B:
        // Pan the timeline
        case timelineHasInteraction && Boolean(timelineInteractionInitialX):
          // calculate what the new viewbox offset should be. this value scrolls the view area.
          const calcViewboxOffsetX: number =
            timelinePanX + timelineInteractionInitialX - clientX;

          // invoke method to pan the timeline
          panTimeline(calcViewboxOffsetX, scaledTimelineWidth, svgRef);

          break;
      }
    },
    [
      extractClientXFromInteractionEvent,
      indicatorHasInteraction,
      timestampDictionary,
      scaledTimelineWidth,
      svgRef,
      timelineHasInteraction,
      timelineInteractionInitialX,
      timelinePanX,
      precinctMarkerDictionary,
      timestampActionsDispatch,
    ]
  );

  // Handler for releasing interaction of the timeline. Reset store values holding
  // offset positioning and interaction flag.
  const handleTimelineInteractionEnd = useCallback(() => {
    // clear any interaction details kept in the store
    timelineActionsDispatch({
      type: DispatchActions.updateTimeline,
      details: {
        timelineHasInteraction: false,
        timelinePanX: svgRef?.current?.viewBox.baseVal?.x || 0,
        timelineInteractionInitialX: undefined,
      },
    } as ActionType.UpdateTimeline);

    if (currentTimestampPositionX && indicatorHasInteraction.current) {
      const markerKey: number = currentTimestampPositionX;

      const { timestamp } = timestampDictionary[markerKey] || {};

      if (timestamp) {
        timelineActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            focusTransition: true,
          },
        } as ActionType.UpdateTimeline);

        timestampActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            currentTimestamp: timestamp,
          },
        } as ActionType.UpdateTimeline);
      }
    }

    // clear the interaction flag for the indicator
    indicatorHasInteraction.current = false;
  }, [
    currentTimestampPositionX,
    indicatorHasInteraction,
    timestampDictionary,
    svgRef,
    timelineActionsDispatch,
    timestampActionsDispatch,
  ]);

  // Attach window level handlers so that dragging can commence if
  // the mouse position leaves the wrapper after the interaction began
  useEffect(
    function initNativeEventListeners() {
      const { current } = svgRef;

      window.addEventListener("mousemove", handleMouseMove);
      window.addEventListener("mouseup", handleTimelineInteractionEnd);
      window.addEventListener("touchmove", handlePanTimeline);
      window.addEventListener("touchend", handleTimelineInteractionEnd);
      // add this explicitly via 'addEventListener' to set passive flag to false.
      // this allows us to negate bubbling to the window level to prevent default
      // behavior that might otherwise be handled on the browser.
      if (current) {
        current.addEventListener("wheel", handleMouseWheel, {
          passive: false,
        });
      }

      // release the listeners when component is unmounted
      return () => {
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("mouseup", handleTimelineInteractionEnd);
        window.removeEventListener("touchmove", handlePanTimeline);
        window.removeEventListener("touchend", handleTimelineInteractionEnd);
        current?.removeEventListener("wheel", handleMouseWheel);
      };
    },
    [
      svgRef,
      handleMouseMove,
      handleTimelineInteractionEnd,
      handleMouseWheel,
      handlePanTimeline,
    ]
  );

  // When the precinct timestamp list changes, update the markers in the timeline
  // to reflect status changes visually
  useLayoutEffect(
    function updateTimestampMarkers() {
      if (svgRef.current) {
        const lineElements = svgRef.current.querySelectorAll(
          `.${styles["timeline-element-group"]}`
        );

        Array.prototype.slice
          .call(lineElements)
          .forEach(function (lineGroup: SVGElement) {
            const { timestamp } = lineGroup.dataset;

            if (
              timestamp &&
              timestampsForPrecinct &&
              !timestampsForPrecinct.includes(timestamp)
            ) {
              lineGroup.classList.add(styles["disabled"]);
              lineGroup.setAttribute("tabindex", "-1");
              lineGroup.setAttribute("data-disabled", "true");
            } else {
              lineGroup.classList.remove(styles["disabled"]);
              lineGroup.setAttribute("tabindex", "0");
              lineGroup.removeAttribute("data-disabled");
            }
          });
      }
    },
    [svgRef, timestampsForPrecinct]
  );

  // detect when the current timestamp value changes, then center the timeline viewbox
  // and set the timestamp element currentStateAbbr to appear active
  useLayoutEffect(
    function selectCurrentTimestampMarker() {
      if (svgRef.current && currentTimestamp) {
        let position: number | undefined = undefined;

        for (let positionKey in timestampDictionary) {
          const { timestamp } = timestampDictionary[positionKey];

          if (timestamp === currentTimestamp) {
            position = parseFloat(positionKey);

            break;
          }
        }

        if (position) {
          centerViewboxOnTimestampMarker(
            position,
            svgRef.current,
            scaledTimelineWidth,
            { transition: focusTransition }
          ).then(function () {
            // Clear any transition flags in the store to eliminate unwanted
            // scrolling effects
            timelineActionsDispatch({
              type: DispatchActions.updateTimeline,
              details: {
                focusTransition: false,
              },
            } as ActionType.UpdateTimeline);
          });

          // Update the current timespan position in the store. This will
          // update the indicator position on the timeline.
          timestampActionsDispatch({
            type: DispatchActions.updateTimeline,
            details: {
              currentTimestampPositionX: position,
            },
          } as ActionType.UpdateTimeline);
        }
      }
    },
    [
      currentTimestamp,
      focusTransition,
      timestampDictionary,
      svgRef,
      scaledTimelineWidth,
      timelineActionsDispatch,
      timestampActionsDispatch,
    ]
  );

  // renderer
  return (
    <section className={containerCx}>
      <svg
        ref={svgRef}
        className={styles["timeline-svg"]}
        onMouseDown={handleTimelineInteractionStart}
        onTouchStart={handleTimelineInteractionStart}
        preserveAspectRatio="xMaxYMax slice"
      >
        <rect
          x={-overscrollThreshold}
          width={trackWidth}
          className={cx(styles["timeline-track"], styles["top"])}
        ></rect>
        <rect
          x={-overscrollThreshold}
          width={trackWidth}
          className={cx(styles["timeline-track"], styles["bottom"])}
        ></rect>
        <TimelineRuler />
        <TimelineMarkers timestampDictionary={timestampDictionary} />
        <g
          ref={indicatorRef}
          onMouseDown={handleIndicatorMouseDown}
          onTouchStart={handleIndicatorMouseDown}
        >
          <TimelineIndicator />
        </g>
      </svg>
    </section>
  );
}
