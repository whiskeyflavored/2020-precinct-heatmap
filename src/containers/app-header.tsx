import React from "react";
import styles from "./app-header.module.scss";

export interface AppHeaderProps {
  children?: React.ReactNode | React.ReactElement[] | React.ReactElement;
}

export function AppHeader({ children }: AppHeaderProps) {
  // render component
  return (
    <header className={styles["app-header"]}>
      <h1 className={styles["app-title"]}>
        <strong>Kraken Map 2020</strong>
      </h1>
      <aside className={styles["state-select"]}>{children}</aside>
    </header>
  );
}
