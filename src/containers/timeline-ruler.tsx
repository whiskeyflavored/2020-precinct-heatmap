import React, { useContext, useEffect, useRef, useMemo } from "react";
import cx from "classnames";
import styles from "./timeline.module.scss";
import { TimelineContext } from "../stores/timeline.store";
import { extractDateStringFromTimestamp } from "../etc/leaflet-helpers";
import { timelineSizes } from "../etc/constants";

// how many milliseconds exist in a day, used for offsetting time markers
// in the ruler component.
const msInADay: number = 86400000;

export function TimelineRuler() {
  const {
    timelineRuleCount,
    timelineScale,
    timelineStartTime,
    timelineWidth,
  } = useContext(TimelineContext);

  const svgRef: React.RefObject<SVGSVGElement> = useRef(null);
  /*
  Build out the markers and labels which inform where on the timeline
  other markers fall in relation to. Adds elements to a collection held in currentStateAbbr.
  */
  const timelineRuler = useMemo(
    function renderTimelineComponent() {
      // a constant representing how many pixels an 'hour' segment should take in the timeline
      const { timeUnitSize, rulerHeight, rulerLabelSize } = timelineSizes;

      // will hold the Rule elements to be added to the SVG element
      const verticalRuleEls: React.ReactElement[] = [];

      // wil; hold the Time Label elements to be added to the SVG element
      const dateLabelEls: React.ReactElement[] = [];

      // iterate through total number of hours in the timeline
      for (let i = 0; i < timelineRuleCount + 1; i++) {
        // calculate where the x offset should be on the timeline for this rule
        const deltaX: number = i * timeUnitSize * timelineScale;

        // Render the next Label text to be rendered on the timeline representing reference dates
        if (!(i % 24)) {
          // calculate which day into the timeline this represents
          const dayCount: number = Math.floor(i / 24);

          // construct a Date object from the day iteration
          const labelDate: Date = new Date(
            timelineStartTime.getTime() + dayCount * msInADay
          );

          // format the label date into a human-readable value
          const labelValue: string = extractDateStringFromTimestamp(labelDate, {
            month: "short",
          });

          // calculate the X position of the label
          const labelPositionX: number = deltaX + 8;

          // calculate the Y position of the label
          const labelPositionY: number = rulerHeight - rulerLabelSize / 2;

          // construct the label as a react element and assign derived attributes
          const labelElement: React.ReactElement = (
            <text
              className={styles["timeline-rule-label"]}
              key={`${labelValue}-${labelPositionX}`}
              x={labelPositionX}
              y={labelPositionY}
            >
              {labelValue}
            </text>
          );

          // push a new label to the collection to be injected into the SVG
          dateLabelEls.push(labelElement);
        }

        // Render the vertical rule line for this hour mark on the timeline from the derived values
        {
          // attribute special class designations to style special time of day markers
          const ruleCx: string = cx(styles["timeline-rule-mark"], {
            [styles.demi]: !(i % 12),
            [styles.strong]: !(i % 24),
          });

          // create the element that will be injected into the SVG
          const ruleElement: React.ReactElement = (
            <line
              key={`hrRule-${i}`}
              x1={deltaX}
              y1="0"
              x2={deltaX}
              y2={rulerHeight}
              className={ruleCx}
            ></line>
          );

          // push the element to the rule element collection
          verticalRuleEls.push(ruleElement);
        }
      }

      return (
        <svg>
          {verticalRuleEls}
          {dateLabelEls}
        </svg>
      );
    },
    [timelineRuleCount, timelineScale, timelineStartTime]
  );

  useEffect(
    function resizeSvg() {
      if (svgRef.current && timelineWidth) {
        svgRef.current.setAttribute("width", timelineWidth);
      }
    },
    [svgRef, timelineWidth]
  );

  return <svg ref={svgRef}>{timelineRuler}</svg>;
}
