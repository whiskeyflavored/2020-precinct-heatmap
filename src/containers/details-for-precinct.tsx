import React, { useCallback, useContext } from "react";
import cx from "classnames";
// import components and containers
import { Icon } from "ts-react-feather-icons";
import { DetailsPanelHeader } from "./details-panel-header";
import { DefinitionList } from "../components/definition-list";
import { TimestampDifferentialList } from "./differential-list-timestamps";
import { VoteListByType } from "./votes-list-by-type";
// import style modules
import sharedStyles from "./details-panel.module.scss";
import styles from "./details-for-precinct.module.scss";
// import store contexts
import { ActionType, DispatchActions } from "../stores/types";
import {
  AvailableLocalesContext,
  CurrentLocaleContext,
  CurrentLocaleActionsContext,
} from "../stores/locale.store";
import { TimestampActionsContext } from "../stores/timeline.store";
import { VotingDataContext } from "../stores/voting.store";

export function PrecinctDetailsPanelLayer() {
  // Dictionary of precinct & county level info for this US state
  const { stateLevelInfo } = useContext(AvailableLocalesContext);

  // Current locality information from the store
  const {
    currentCountyName,
    currentStateAbbr,
    currentPrecinctName,
  } = useContext(CurrentLocaleContext);

  // Look up the full state name from the static state-level info
  const currentStateName: string | undefined =
    stateLevelInfo[currentStateAbbr]["name"];

  const { votingDataForPrecinctAtTimestamp } = useContext(VotingDataContext);

  // Dispatch actions to update the locale store
  const currentLocaleActionsDispatch = useContext(CurrentLocaleActionsContext);

  // Dispatch actions to update timestamps in the store.
  const timestampActionsDispatch = useContext(TimestampActionsContext);

  // Derive a subtext to include in the header from current locale information.
  // Indicates the county and state given the current selected precinct.
  const localeDescription: string = `${currentCountyName} county, ${currentStateName}`;

  const demPartyMarkerCx: string = cx(
    sharedStyles["party-marker"],
    sharedStyles["dem"]
  );

  const gopPartyMarkerCx: string = cx(
    sharedStyles["party-marker"],
    sharedStyles["gop"]
  );

  // Reset the store to clear any selected precinct and related information
  const handleClearPrecinct = useCallback(
    function () {
      // Reset locale info in the store
      currentLocaleActionsDispatch({
        type: DispatchActions.updateCurrentLocale,
        details: {
          currentPrecinctName: undefined,
          currentPrecinctId: undefined,
          currentCountyName: undefined,
        },
      } as ActionType.UpdateCurrentLocale);

      // Any timestamps currently in store should also be purged
      timestampActionsDispatch({
        type: DispatchActions.updateTimeline,
        details: {
          timestampsForPrecinct: undefined,
        },
      } as ActionType.UpdateTimeline);
    },
    [currentLocaleActionsDispatch, timestampActionsDispatch]
  );

  return (
    <div
      className={cx(sharedStyles["details-panel"], styles["precinct-details"])}
    >
      <DetailsPanelHeader
        title={currentPrecinctName}
        subtext={localeDescription}
      ></DetailsPanelHeader>
      <nav className={styles["nav-wrapper"]}>
        <button className={styles["back-button"]} onClick={handleClearPrecinct}>
          <Icon name="arrow-left-circle" size={16}></Icon>
          <span>View timestamp details</span>
        </button>
      </nav>
      <section className={cx(sharedStyles["body"], styles["body"])}>
        <section className={sharedStyles["topic"]}>
          <h3>
            <strong>Votes for Biden</strong>
            <div className={demPartyMarkerCx}></div>
          </h3>
          <DefinitionList>
            <VoteListByType
              votingData={votingDataForPrecinctAtTimestamp}
              candidate="biden"
            />
          </DefinitionList>
          <h3>
            <strong>Votes for Trump</strong>
            <div className={gopPartyMarkerCx}></div>
          </h3>
          <DefinitionList>
            <VoteListByType
              votingData={votingDataForPrecinctAtTimestamp}
              candidate="trump"
            />
          </DefinitionList>
          <h3>
            <strong>Reporting History</strong>
          </h3>
          <TimestampDifferentialList />
        </section>
      </section>
    </div>
  );
}
