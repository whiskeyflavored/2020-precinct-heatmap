import React, {
  useCallback,
  useContext,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import Leaflet from "leaflet";
import cx from "classnames";
import styles from "./chloropleth.module.scss";
import { ActionType, DispatchActions } from "../stores/types";
import { ApplicationActionsContext } from "../stores/application.store";
import { VotingDataContext } from "../stores/voting.store";
import {
  AvailableLocalesContext,
  CurrentLocaleContext,
  CurrentLocaleActionsContext,
} from "../stores/locale.store";
import { MapContext, MapPopoverActionsContext } from "../stores/map.store";
import { MapPopover } from "../components/map-popover";

import {
  clearInteractiveLayerStyling,
  clearShapeStyling,
  colorizeMapFeature,
} from "../etc/leaflet-helpers";
import "./chloropleth.scss";


// shape of our Chloropleth component props
interface ChloroplethProps {
  children?: React.ReactNode;
  className?: string;
}

// expose this component as a module
export function Chloropleth({
  children,
  className,
}: ChloroplethProps): React.ReactElement {
  // interaction state of the map
  const hasInteraction: React.MutableRefObject<boolean> = useRef(false);

  // hold reference to the map layer currently selected via interaction
  const [selectedLayer, setSelectedLayer] = useState<any | undefined>();

  // React ref hooks
  const leafletMapElement: React.RefObject<HTMLDivElement> = useRef(null);

  const leafletMap: React.MutableRefObject<any | undefined> = useRef();

  const geoJsonRef: React.MutableRefObject<
    Leaflet.GeoJSON | undefined
  > = useRef();

  const appActionsDispatch = useContext(ApplicationActionsContext);

  const { availablePrecincts, stateLevelInfo } = useContext(
    AvailableLocalesContext
  );

  const { currentStateAbbr, currentPrecinctId } = useContext(
    CurrentLocaleContext
  );

  const { geoJsonPrecincts } = useContext(MapContext);

  const { votingDataForStateAtTimestamp } = useContext(VotingDataContext);

  const currentLocaleActionsDispatch = useContext(CurrentLocaleActionsContext);

  const mapContextDispatch = useContext(MapPopoverActionsContext);

  // compoet container classname
  const mapCx: string = cx(styles["map-container"], {
    className: Boolean(className),
  });

  // When a user intearcts with a map feature, set the interaction flag to true
  const handleFeatureMousedown = useCallback(function () {
    hasInteraction.current = true;
  }, []);

  // We want to ignore false 'clicks' when the user is simply attempting to
  // pan the map. When the mouse has moved, set the interaction flag back to false.
  // This ensures that clicks which are interpreted during the selection process are genuine.
  const handleFeatureMousemove = useCallback(function () {
    hasInteraction.current = false;
  }, []);

  // hide any map popovers when mouseout event is bubbled up to the map element
  const handleFeatureMouseOut = useCallback(
    function hideMapPopover() {
      mapContextDispatch({
        type: DispatchActions.updateMapState,
        details: {
          isMapPopoverVisible: false,
        },
      } as ActionType.UpdateMapState);
    },
    [mapContextDispatch]
  );

  // callback function to display geographic layer information
  // onto the map given a certain interaction.
  const handleFeatureMouseOver = useCallback(
    function setMapPopoverData(event: Leaflet.LeafletMouseEvent) {
      const layerEl: SVGElement = event.target.getElement();
      // destructure feature properties from the event target (map layer)
      const { countyName, precinctName } =
        event.target.feature?.properties || {};

      // capture the current feature properties in the store
      if (countyName && precinctName) {
        // pop the hovered layer to the top of the rendering stack
        layerEl?.parentNode?.appendChild(layerEl);

        // update the store to reflect the interacted layer properties
        mapContextDispatch({
          type: DispatchActions.updateMapState,
          details: {
            isMapPopoverVisible: true,
            mapPopoverCountyName: countyName,
            mapPopoverPositionX: event.containerPoint.x,
            mapPopoverPositionY: event.containerPoint.y,
            mapPopoverPrecinctName: precinctName,
          },
        } as ActionType.UpdateMapState);
      }
    },
    [mapContextDispatch]
  );

  // Capture a feature into state as 'selected'.
  const handleFeatureSelection = useCallback(
    function selectFeature(event: Leaflet.LeafletMouseEvent) {
      // Only continue if this was a purposeful selection gesture/action
      if (hasInteraction.current) {
        // reset the interaction flag
        hasInteraction.current = false;

        // grab a reference to the layer from the Leaflet event target
        const layer: any = event.target;

        // store the extracted layer reference into state
        setSelectedLayer(layer);

        // destructure the properties props that have been assigned to this
        // layer's feature during instantiation.
        const { geo_id, countyName, precinctName } =
          layer.feature?.properties || {};

        // capture the current locale details from the selected layer into the store
        currentLocaleActionsDispatch({
          type: DispatchActions.updateCurrentLocale,
          details: {
            currentCountyName: countyName,
            currentPrecinctId: geo_id,
            currentPrecinctName: precinctName,
          },
        } as ActionType.UpdateCurrentLocale);
      }
    },
    [currentLocaleActionsDispatch, hasInteraction, setSelectedLayer]
  );

  // removes class from any features on the map when a new feature is selected
  // as provided by helper function `clearInteractiveLayerStyling`
  useEffect(
    function clearSelectedLayerStyles() {
      if (leafletMapElement && selectedLayer) {
        // remove any outstanding selected layers in the map
        clearInteractiveLayerStyling("selected");

        // add new class string to the list to apply styling to this layer
        selectedLayer.getElement().classList.add("selected");
      }
    },
    [selectedLayer, leafletMapElement]
  );

  // Initialize Leaflet Map instance and contain that erference in App state.
  // The Map instance will be reference-able to access all children elements/objects
  // contained which may be necessary to manipulate the visualization.
  useEffect(
    function renderGeoJsonToMap() {
      // conditions are that we have the root 'container' element recognized,
      // geojson data has been fetched, and no existing map instance currently exists.
      if (
        leafletMapElement.current &&
        availablePrecincts &&
        geoJsonPrecincts &&
        stateLevelInfo
      ) {
        if (!leafletMap.current) {
          // create our Leaflet Map instance using default coords and zoom level
          const map: any = Leaflet.map(leafletMapElement.current, {
            zoomControl: false,
          });

          // set zoom controls with custom positioning
          Leaflet.control.zoom({ position: "topright" }).addTo(map);

          // update reference to contain the new instance ref
          leafletMap.current = map;
        }

        // clear any existing geoJson layer items kept in memory
        geoJsonRef.current?.clearLayers();

        // create a geojson layer with the complete data and options. attach to map instance
        geoJsonRef.current = Leaflet.geoJSON(geoJsonPrecincts, {
          onEachFeature: (feature: any, layer: any) => {
            availablePrecincts.some((dictItem: any) => {
              return dictItem.geo_id === feature.properties.geo_id
                ? // assign proper human-readable values designating locality names to
                  // be included in the feature-level properties.
                  ((feature.id = dictItem.geo_id),
                  (feature.properties.precinctName = dictItem.locality_name),
                  (feature.properties.countyName =
                    dictItem.locality_parent_name),
                  true)
                : false;
            });
            // instantiate event handlers on creation of this feature layer
            layer.on({
              mousedown: handleFeatureMousedown,
              mousemove: handleFeatureMousemove,
              mouseup: handleFeatureSelection,
              mouseover: handleFeatureMouseOver,
            });
          },
        }).addTo(leafletMap.current);

        // center the view to the current state centering
        leafletMap.current.setView(
          stateLevelInfo[currentStateAbbr].centerCoords, // map center
          7 // zoom factor
        );
      }
    },
    [
      leafletMapElement,
      leafletMap,
      availablePrecincts,
      currentStateAbbr,
      geoJsonPrecincts,
      geoJsonRef,
      stateLevelInfo,
      handleFeatureSelection,
      handleFeatureMouseOver,
      handleFeatureMousedown,
      handleFeatureMousemove
    ]
  );

  /*
  Once all data is available and map instance has been created, 
  begin to visualize the data by coloring shapes in map via Leaflet API
  to allow for granular control of styling (applied CSS?)
  */
  useEffect(
    function colorizeChloroplethByVoterData() {
      //proceed only if we have necessary data
      if (
        leafletMap.current &&
        leafletMapElement.current &&
        geoJsonPrecincts && 
        votingDataForStateAtTimestamp
      ) {
        // remove existing styling from shapes on map
        clearShapeStyling(leafletMapElement.current);

        // iterate through precincts to extract precinctNames (used as keys within precincts)
        for (let row of votingDataForStateAtTimestamp) {
          // iterate through the map layers to check against the current precinct (by name)
          // NOTE: there needs to be better data synchronization here. There are some missing
          // or mismatched names, and very little manner to decipher how they correlate based
          // on the limited data we have in the time series CSV file.
          for (let layerKey in leafletMap.current._layers) {
            // quick reference to the current layer
            const layer: any = leafletMap.current._layers[layerKey];

            if (layer.feature) {
              // destructure layer county
              const { geo_id } = layer.feature.properties || {};

              // Compare these to look if precinct name is contained within the layer name.
              // This is a HACK method to compare. should be done by synched identifier/UUID
              if (geo_id === row.geo_id) {
                colorizeMapFeature(row, layer);

                break;
              }
            }
          }
        }

        // claer the loading curtain
        appActionsDispatch({
          type: DispatchActions.updateFetchStatus,
          details: {
            isFetching: false,
            process: ["fetch geoJson", "render map"],
          },
        } as ActionType.UpdateFetchStatus);
      }
    },
    [
      appActionsDispatch,
      geoJsonPrecincts,
      leafletMap,
      leafletMapElement,
      votingDataForStateAtTimestamp,
    ]
  );

  useLayoutEffect(
    function updateLayerSelection() {
      if (leafletMap.current) {
        // when precinct & county prop is present and have changed,
        // iterate through layers to find the layer to select.
        if (currentPrecinctId) {
          for (let layerKey in leafletMap.current._layers) {
            const layer: any = leafletMap.current._layers[layerKey];

            const { geo_id } = layer.feature?.properties || {};

            // compare precinct & county names to determine a match
            if (geo_id === currentPrecinctId) {
              // highlight the layer if a match is found
              setSelectedLayer(layer);

              // stop further iteration through the loop as it is no longer necessary
              break;
            }
          }
        }

        // if we update with undefined precinct & county, clear selection states
        else {
          setSelectedLayer(undefined);

          clearInteractiveLayerStyling("selected");
        }
      }
    },
    [leafletMap, currentPrecinctId]
  );

  // return a simple wrapper for our Leaflet Map container
  return (
    <div
      ref={leafletMapElement}
      className={mapCx}
      onMouseOut={handleFeatureMouseOut}
    >
      <MapPopover />
      <div className={styles["locale-controls"]}>{children}</div>
    </div>
  );
}
