import React, { useContext, useMemo } from "react";
import cx from "classnames";
import { DetailsPanelHeader } from "./details-panel-header";
import { PrecinctDifferentialList } from "./differential-list-precincts";
import { DefinitionList } from "../components/definition-list";
import { TimestampContext } from "../stores/timeline.store";
import { VoteListByType } from "./votes-list-by-type";
import sharedStyles from "./details-panel.module.scss";
import {
  convertTimestampToLocalTime,
  extractDateStringFromTimestamp,
} from "../etc/leaflet-helpers";
import { VotingDataContext } from "../stores/voting.store";

export function TimestampDetailsPanel() {
  // Current selected timestamp from the store
  const { currentTimestamp } = useContext(TimestampContext);

  // Current voting data for the state at the current timestamp held in the store
  const { votingDataForStateAtTimestamp } = useContext(VotingDataContext);

  const demPartyMarkerCx: string = cx(
    sharedStyles["party-marker"],
    sharedStyles["dem"]
  );

  const gopPartyMarkerCx: string = cx(
    sharedStyles["party-marker"],
    sharedStyles["gop"]
  );

  // Create & memoize subtext as the current timestamp in human-readable format
  // to be placed in the details panel header container.
  const subtextEl: React.ReactNode = useMemo(
    function () {
      if (currentTimestamp) {
        // convert the timestamp to local time
        const timestampDate: Date = convertTimestampToLocalTime(
          currentTimestamp
        );

        return (
          <span className={sharedStyles["timestamp-label"]}>
            <time dateTime={timestampDate.toLocaleDateString()}>
              {extractDateStringFromTimestamp(timestampDate, { month: "long" })}
            </time>
            <span> — </span>
            <time dateTime={timestampDate.toLocaleTimeString()}>
              {timestampDate.toLocaleTimeString()}
            </time>
          </span>
        );
      }
    },
    [currentTimestamp]
  );

  return (
    <aside className={sharedStyles["details-panel"]}>
      <DetailsPanelHeader
        title="Timestamp Report"
        subtext={subtextEl}
      ></DetailsPanelHeader>
      <div className={sharedStyles["body"]}>
        <section className={sharedStyles["topic"]}>
          <h3>
            <strong>Votes for Biden</strong>
            <div className={demPartyMarkerCx}></div>
          </h3>
          <DefinitionList>
            <VoteListByType
              candidate="biden"
              votingData={votingDataForStateAtTimestamp}
            />
          </DefinitionList>
          <h3>
            <strong>Votes for Trump</strong>
            <div className={gopPartyMarkerCx}></div>
          </h3>
          <DefinitionList>
            <VoteListByType
              candidate="trump"
              votingData={votingDataForStateAtTimestamp}
            />
          </DefinitionList>
          <h3>
            <strong>Reporting precincts</strong>
          </h3>
          <PrecinctDifferentialList />
        </section>
        <section className={sharedStyles["topic"]}></section>
      </div>
    </aside>
  );
}
