import React, { useCallback, useContext, useMemo } from "react";
import cx from "classnames";
import { Icon } from "ts-react-feather-icons";
import { Select } from "../components/select";
import {
  convertTimestampToLocalTime,
  extractDateStringFromTimestamp,
} from "../etc/leaflet-helpers";
import styles from "./timeline-controls.module.scss";
// import store contexts
import { ActionType, DispatchActions } from "../stores/types";
import {
  TimelineContext,
  TimelineActionsContext,
  TimestampContext,
  TimestampActionsContext,
} from "../stores/timeline.store";

const iconSize: number = 12;

interface TimelineControlBarProps {
  className?: string;
}

export function TimelineControlBar({
  className,
}: TimelineControlBarProps): React.ReactElement {
  const { timelineScale, zoomMin, zoomMax } = useContext(TimelineContext);

  const {
    currentTimestamp,
    timestampsForPrecinct,
    timestampsForState,
  } = useContext(TimestampContext);

  const timelineActionsDispatch = useContext(TimelineActionsContext);

  const timestampActionsDispatch = useContext(TimestampActionsContext);

  // determine which timestamp should be used in consideration of
  const useTimestampList: string[] | undefined = timestampsForPrecinct?.length
    ? timestampsForPrecinct
    : timestampsForState?.length
    ? timestampsForState
    : undefined;

  // set attribute status values for timestamp navigation buttons
  const statusDisablePreviousBtn: boolean =
    useTimestampList && useTimestampList[0] !== currentTimestamp ? false : true;

  const statusDisableNextBtn: boolean =
    useTimestampList &&
    useTimestampList[useTimestampList.length - 1] !== currentTimestamp
      ? false
      : true;

  const statusDisableZoomOutBtn: boolean = timelineScale <= zoomMin;

  const statusDisableZoomInBtn: boolean = timelineScale >= zoomMax;

  const wrapperCx: string = cx(styles["timeline-controls"], {
    className: Boolean(className),
  });

  const handleChange = useCallback(
    function (timestamp: string) {
      console.log(timestamp);
      timelineActionsDispatch({
        type: DispatchActions.updateTimeline,
        details: {
          focusTransition: true,
        },
      } as ActionType.UpdateTimeline);

      timestampActionsDispatch({
        type: DispatchActions.updateTimeline,
        details: {
          currentTimestamp: timestamp,
        },
      } as ActionType.UpdateTimeline);
    },
    [timelineActionsDispatch, timestampActionsDispatch]
  );

  const handleNavAction = useCallback(
    (event: React.MouseEvent) => {
      const useTimestampList: string[] | undefined =
        timestampsForPrecinct || timestampsForState;

      if (useTimestampList && currentTimestamp) {
        const target: any = event.target;

        const buttonEl: HTMLButtonElement | undefined = (function matchParent(
          target: Element
        ): HTMLButtonElement | undefined {
          if (target.parentNode) {
            return target.parentNode &&
              target.tagName.toLowerCase() === "button"
              ? (target as HTMLButtonElement)
              : matchParent(target.parentNode as Element);
          } else {
            return undefined;
          }
        })(target);

        const action: string | null | undefined = buttonEl?.getAttribute(
          "data-action"
        );

        const currentStampIndex: number = useTimestampList.indexOf(
          currentTimestamp
        );

        if (currentStampIndex >= 0) {
          let targetTimestampIndex: number = currentStampIndex;

          switch (action) {
            // Navigate to the first timestamp in the timeseries list
            case "first":
              const firstStampIndex: number = useTimestampList.indexOf(
                useTimestampList[0]
              );

              targetTimestampIndex = firstStampIndex;

              break;

            // Navigate to the previous timestamp in the current timeseries list
            case "previous":
              const prevStampIndex: number = useTimestampList.indexOf(
                useTimestampList[currentStampIndex - 1]
              );

              targetTimestampIndex = prevStampIndex;

              break;

            // Navigate to the next timestamp in the current timestamp list
            case "next":
              const nextStampIndex: number = useTimestampList.indexOf(
                useTimestampList[currentStampIndex + 1]
              );

              targetTimestampIndex = nextStampIndex;

              break;

            // Navigate to the last timestamp in the current tieeseries list
            case "last":
              targetTimestampIndex = useTimestampList.indexOf(
                useTimestampList[useTimestampList.length - 1]
              );

              break;
          }

          // Update the store if the target index exists in the current timeseries list
          if (useTimestampList[targetTimestampIndex] && !target.disabled) {
            // make sure the timeline animates the next transition to the new timestamp
            timelineActionsDispatch({
              type: DispatchActions.updateTimeline,
              details: {
                focusTransition: true,
              },
            } as ActionType.UpdateTimeline);

            // Update the timestamp in the store
            timestampActionsDispatch({
              type: DispatchActions.updateTimeline,
              details: {
                currentTimestamp: useTimestampList[targetTimestampIndex],
              },
            } as ActionType.UpdateTimeline);
          }
        }
      }
    },
    [
      currentTimestamp,
      timelineActionsDispatch,
      timestampActionsDispatch,
      timestampsForPrecinct,
      timestampsForState,
    ]
  );

  const handleZoom = useCallback(
    function (event: React.MouseEvent) {
      const target: Element = event.target as Element;

      const buttonEl: HTMLButtonElement | undefined = (function matchParent(
        target: Element
      ): HTMLButtonElement | undefined {
        if (target.parentNode) {
          return target.parentNode && target.tagName.toLowerCase() === "button"
            ? (target as HTMLButtonElement)
            : matchParent(target.parentNode as Element);
        } else {
          return undefined;
        }
      })(target);

      if (buttonEl) {
        const { action } = buttonEl.dataset;

        let newTimelineScale: number =
          action === "zoom-in"
            ? timelineScale + 1 > zoomMax
              ? zoomMax
              : timelineScale + 1
            : timelineScale - 1 < zoomMin
            ? zoomMin
            : timelineScale - 1;

        timelineActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            timelineScale: newTimelineScale,
          },
        } as ActionType.UpdateTimeline);
      }
    },
    [timelineScale, timelineActionsDispatch, zoomMin, zoomMax]
  );

  const optionsList = useMemo(
    function renderOptions() {
      const iterateList: any[] | undefined =
        timestampsForPrecinct || timestampsForState;

      return iterateList?.map(
        (timestamp: string): React.ReactElement<HTMLOptionElement> => {
          const selectedDate: Date = convertTimestampToLocalTime(timestamp);

          const dateString: string = extractDateStringFromTimestamp(
            selectedDate
          );

          const timeString: string = selectedDate.toLocaleTimeString(
            "default",
            { timeZoneName: "short" }
          );

          const isDisabled: boolean = Boolean(
            timestampsForPrecinct && !timestampsForPrecinct?.includes(timestamp)
          );

          return (
            <option value={timestamp} key={timestamp} disabled={isDisabled}>
              {`${dateString} - ${timeString}`}
            </option>
          );
        }
      );
    },
    [timestampsForPrecinct, timestampsForState]
  );

  return (
    <div className={wrapperCx}>
      <section className={styles["span-width"]} />
      <section className={styles["time-controls"]}>
        <div className={styles["button-group"]}>
          <button
            data-action="first"
            disabled={statusDisablePreviousBtn}
            onClick={handleNavAction}
          >
            <Icon name="chevrons-left" size={iconSize} />
          </button>
          <button
            data-action="previous"
            disabled={statusDisablePreviousBtn}
            onClick={handleNavAction}
          >
            <Icon name="chevron-left" size={iconSize} />
          </button>
        </div>
        <Select
          defaultValue={currentTimestamp}
          value={currentTimestamp}
          onChange={handleChange}
        >
          {optionsList}
        </Select>
        <div className={styles["button-group"]}>
          <button
            data-action="next"
            disabled={statusDisableNextBtn}
            onClick={handleNavAction}
          >
            <Icon name="chevron-right" size={iconSize} />
          </button>
          <button
            data-action="last"
            disabled={statusDisableNextBtn}
            onClick={handleNavAction}
          >
            <Icon name="chevrons-right" size={iconSize} />
          </button>
        </div>
      </section>
      <section className={styles["zoom-controls"]}>
        <div className={styles["button-group"]}>
          <button
            data-action="zoom-out"
            disabled={statusDisableZoomOutBtn}
            onClick={handleZoom}
          >
            <Icon name="minus" size={iconSize} />
          </button>
          <button
            data-action="zoom-in"
            disabled={statusDisableZoomInBtn}
            onClick={handleZoom}
          >
            <Icon name="plus" size={iconSize} />
          </button>
        </div>
      </section>
    </div>
  );
}
