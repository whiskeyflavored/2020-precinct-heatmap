import React, { useCallback, useContext, useMemo } from "react";
import { Select } from "../components/select";
import { ActionType, DispatchActions } from "../stores/types";
import {
  AvailableLocalesContext,
  CurrentLocaleContext,
  CurrentLocaleActionsContext,
} from "../stores/locale.store";
import { StateDetails } from "../etc/geographic-data";
import styles from "./locale-selectors.module.scss";

export function LocaleSelectors() {
  // grab context calues for applicable localities
  const {
    availableStates,
    availableCounties,
    availablePrecincts,
    stateLevelInfo,
  } = useContext(AvailableLocalesContext);

  const {
    currentStateAbbr,
    currentCountyId,
    currentCountyName,
    currentPrecinctId,
    currentPrecinctName,
  } = useContext(CurrentLocaleContext);

  const currentLocaleActionsDispatch = useContext(CurrentLocaleActionsContext);

  const renderOptions = useCallback(function (
    optionList: any[]
  ): React.ReactElement[] {
    const defaultOption: React.ReactElement<HTMLOptionElement> = (
      <option key="default" value="default" disabled></option>
    );

    const optionElementList: Array<
      React.ReactElement<HTMLOptionElement>
    > = optionList
      ?.map(({ geo_id, locality_name }: any) => {
        // Return an option element if these details exist
        return geo_id && locality_name ? (
          <option key={`${geo_id}-${locality_name}`} value={geo_id}>
            {locality_name}
          </option>
        ) : null;
      })
      // remove any null items in the list
      .filter((n: unknown) =>
        Boolean(n)
      ) as React.ReactElement<HTMLOptionElement>[];

    return [defaultOption, optionElementList].flat();
  },
  []);

  const onChangeState = useCallback(
    function changeStateHandler(stateAbbr: string) {
      currentLocaleActionsDispatch({
        type: DispatchActions.updateCurrentLocale,
        details: {
          currentStateAbbr: stateAbbr,
        },
      } as ActionType.UpdateCurrentLocale);
    },
    [currentLocaleActionsDispatch]
  );

  const onChangeCounty = useCallback(
    function changeCountyHandler(countyId: string) {
      availableCounties.some((countyData: any) => {
        if (countyData.geo_id === countyId) {
          // Conditional precinct data to include with the payload. If the
          // current precinct is a child of the selected county, don't set any new
          // current precinct values. Otherwise set them to 'undefined'.
          const newPrecinctData: any = countyData.locality_children.includes(
            currentPrecinctId
          )
            ? {}
            : { currentPrecinctId: undefined, currentPrecinctName: undefined };
          // update the store with the new county details
          currentLocaleActionsDispatch({
            type: DispatchActions.updateCurrentLocale,
            details: {
              currentCountyId: countyId,
              currentCountyName: countyData.geo_id,
              // mixin of conditional precinct data
              ...newPrecinctData,
            },
          });
        }

        // break the loop
        return true;
      });
    },
    [availableCounties, currentPrecinctId, currentLocaleActionsDispatch]
  );

  const onChangePrecinct = useCallback(
    function changePrecinctHandler(precinctId: string) {
      availablePrecincts.some(
        ({
          geo_id,
          locality_name,
          locality_parent_name,
          locality_parent_id,
        }: any) => {
          if (geo_id === precinctId) {
            // get the county ID from the available counties info
            for (let countyData of availableCounties) {
              if (countyData.locality_name === locality_parent_name) {
                // update the store with relevant locality details
                currentLocaleActionsDispatch({
                  type: DispatchActions.updateCurrentLocale,
                  details: {
                    currentCountyId: countyData.geo_id,
                    currentCountyName: locality_parent_name,
                    currentPrecinctId: geo_id,
                    currentPrecinctName: locality_name,
                  },
                });
                // break the loop
                break;
              }
            }
            // break the loop
            return true;
          }
        }
      );
    },
    [availableCounties, availablePrecincts, currentLocaleActionsDispatch]
  );

  const renderStateOptions: React.ReactElement[] = useMemo(
    function () {
      return (
        availableStates
          ?.map((stateAbbr: string) => {
            // Compare this state abbreviation to locally defined constants to pull
            // state-level data needed to render
            const stateDetails: StateDetails | undefined =
              stateLevelInfo[stateAbbr];

            // Return an option element if these details exist
            return stateDetails ? (
              <option
                key={stateAbbr}
                value={stateAbbr}
                disabled={!stateDetails}
              >
                {stateDetails.name}
              </option>
            ) : null;
          })
          // remove any null items in the list
          .filter((n: unknown) => Boolean(n))
      );
    },
    [availableStates, stateLevelInfo]
  );

  const renderCountyOptions: React.ReactElement[] | undefined = useMemo(
    function () {
      return renderOptions(availableCounties);
    },
    [availableCounties, renderOptions]
  );

  const renderPrecinctOptions: React.ReactElement[] | undefined = useMemo(
    function () {
      return renderOptions(availablePrecincts);
    },
    [availablePrecincts, renderOptions]
  );

  return (
    <nav className={styles["selectors-wrapper"]}>
      <div className={styles["select-wrapper"]}>
        <Select defaultValue={currentStateAbbr} onChange={onChangeState}>
          {renderStateOptions}
        </Select>
      </div>
      {/*
      <div className={styles["select-wrapper"]}>
        <Select value={currentCountyName} onChange={onChangeCounty} disabled={true}>
          {renderCountyOptions}
        </Select>
      </div>
      <div className={styles["select-wrapper"]}>
        <Select defaultValue={currentPrecinctId} value={currentPrecinctId} onChange={onChangePrecinct}>
          {renderPrecinctOptions}
        </Select>
      </div>*/}
    </nav>
  );
}
