import React, { useCallback, useContext, useMemo } from "react";
import cx from "classnames";
import styles from "./differential-list-precincts.module.scss";
import sharedStyles from "./details-panel.module.scss";
import { CurrentLocaleActionsContext } from "../stores/locale.store";
import { VotingDataContext } from "../stores/voting.store";
import { ActionType, DispatchActions } from "../stores/types";
import { deriveAggregatePartyVotesByType } from "../etc/voting-data-helpers";

export function PrecinctDifferentialList() {
  const { votingDataForStateAtTimestamp } = useContext(VotingDataContext);

  const currentLocaleActionsDispatch = useContext(CurrentLocaleActionsContext);

  const tableCx = cx(
    sharedStyles["details-table"],
    styles["precinct-diff-table"]
  );

  const handlePrecinctSelect = useCallback(
    function (event: React.MouseEvent<HTMLButtonElement>) {
      const target = event.target as HTMLButtonElement;

      const { countyName, precinctId, precinctName } = target.dataset;

      if (countyName && precinctId && precinctName) {
        currentLocaleActionsDispatch({
          type: DispatchActions.updateCurrentLocale,
          details: {
            currentCountyName: countyName,
            currentPrecinctId: precinctId,
            currentPrecinctName: precinctName,
          },
        } as ActionType.UpdateCurrentLocale);
      }
    },
    [currentLocaleActionsDispatch]
  );

  const orderedVotingDataForStateAtTimestamp = useMemo(
    function () {
      if (votingDataForStateAtTimestamp) {
        const sortedResults: any = [];

        for (let {
          geo_id,
          locality_name,
          precinct_id,
          results,
        } of votingDataForStateAtTimestamp) {
          const { demVotes, gopVotes } = deriveAggregatePartyVotesByType(
            results
          );

          const voteDifferential: number = Math.abs(demVotes - gopVotes);

          const newEntryDetails: any = {
            geo_id,
            locality_name,
            precinct_id,
            demVotes,
            gopVotes,
            voteDifferential,
          };

          if (
            !sortedResults.some(function (sortedEntry: any, index: number) {
              if (voteDifferential < sortedEntry.voteDifferential) {
                sortedResults.splice(index, 0, newEntryDetails);
                return true;
              } else {
                return false;
              }
            })
          ) {
            sortedResults.push(newEntryDetails);
          }
        }

        return sortedResults.reverse();
      }
    },
    [votingDataForStateAtTimestamp]
  );

  const tableRows: React.ReactNode = useMemo(
    function (): React.ReactNode {
      return orderedVotingDataForStateAtTimestamp?.map(function ({
        geo_id,
        locality_name,
        precinct_id,
        demVotes,
        gopVotes,
        voteDifferential,
      }: any) {
        const majorityParty: string = demVotes > gopVotes ? "dem" : "gop";
        const partyCx: string = cx(
          sharedStyles["party-marker"],
          sharedStyles[majorityParty]
        );

        return (
          <tr
            key={`${geo_id}-${precinct_id}`}
            className={styles["precinct-row"]}
          >
            <td>
              <button
                data-county-name={locality_name}
                data-precinct-id={geo_id}
                data-precinct-name={precinct_id}
                onClick={handlePrecinctSelect}
              >
                {precinct_id}
              </button>
            </td>
            <td>
              {`+ ${voteDifferential}`}
              <div className={partyCx}></div>
            </td>
          </tr>
        );
      });
    },
    [handlePrecinctSelect, orderedVotingDataForStateAtTimestamp]
  );

  return (
    <table className={tableCx}>
      <thead>
        <tr>
          <th>Precinct name</th>
          <th>Differential</th>
        </tr>
      </thead>
      <tbody>{tableRows}</tbody>
    </table>
  );
}
