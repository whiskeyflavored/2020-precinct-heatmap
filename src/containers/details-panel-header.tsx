import React from "react";
import styles from "./details-panel.module.scss";

interface DetailsPanelHeaderProps {
  subtext?: string | React.ReactElement | React.ReactNode;
  title: string;
}

export function DetailsPanelHeader({
  subtext,
  title,
}: DetailsPanelHeaderProps) {

  // Subtext component if one is provided as a prop
  const subtextElement: React.ReactElement | undefined = subtext ? (
    <span className={styles["subtext"]}>{subtext}</span>
  ) : undefined;

  return (
    <header className={styles["details-panel-header"]}>
      <h2>
        <strong>{title}</strong>
      </h2>
      {subtextElement}
    </header>
  );
}
