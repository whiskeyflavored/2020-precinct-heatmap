import React, { useMemo } from "react";
import styles from "../components/definition-list.module.scss";
import { RatioBar } from "../components/ratio-bar";
import { tallyAllVotesForCandidate } from "../etc/voting-data-helpers";
import { sumVotingTotalsByType } from "../etc/voting-data-helpers";

interface VoteListByTypeProps {
  candidate: string;
  votingData?: any[];
}

export function VoteListByType({ candidate, votingData }: VoteListByTypeProps) {
  const partyName: string = candidate === "biden" ? "dem" : "gop";

  // Iterate through all reporting precincts in the voting data at this timestamp.
  // Sum all of the votes and segregate by type, stored as a dictionary.
  const votingTotals: any = useMemo(
    function () {
      if (votingData) {
        // coerce the voting data to be in an array form (as expected by the helper function
        // if a single precinct-level data entry has been provided.
        // TODO: these need to be defined as response types in the future)
        const useVotingDataForm: any[] =
          votingData instanceof Array ? votingData : [votingData];

        return sumVotingTotalsByType(useVotingDataForm);
      } else {
        return {};
      }
    },
    [votingData]
  );

  // Memoize the derived vote totals aggregated by vote type for Biden across all reporting locales
  const totalVotesBiden = useMemo(
    () => tallyAllVotesForCandidate("biden", votingTotals),
    [votingTotals]
  );

  // Memoize the derived vote totals aggregated by vote type for Trump across all reporting locales
  const totalVotesTrump = useMemo(
    () => tallyAllVotesForCandidate("trump", votingTotals),
    [votingTotals]
  );

  // Derive the total candidate votes for all vot types
  const candidateTotalVotes: number =
    candidate === "biden" ? totalVotesBiden : totalVotesTrump;

  // If there are multiple vote types, include a sum total row. Set
  // a boolean flag to indicate this should be rendered.
  const includeTotalsEntry: boolean = Object.keys(votingTotals).length > 1;

  // Iterate through the derived voting totals and create Definition List entries to be rendered
  const voteTypeRows: React.ReactElement[] = useMemo(
    function () {
      const defList: React.ReactElement[] = [];

      for (let voteType in votingTotals) {
        const value: number = votingTotals[voteType][candidate];

        defList.push(
          <React.Fragment key={`${voteType}-${candidate}`}>
            <dt>{voteType}</dt>
            <dd>{value}</dd>
          </React.Fragment>
        );
      }

      return defList;
    },
    [candidate, votingTotals]
  );

  // Define the sum total of votes component to be insterted at the end of the  Definition List
  const voteSumRow: React.ReactElement = (
    <React.Fragment key={`total-${candidate}`}>
      <dt className={styles["emphasis"]}>total</dt>
      <dd className={styles["emphasis"]}>{candidateTotalVotes}</dd>
    </React.Fragment>
  );

  // Define the percentage ratio entry component to be inserted at the end of the Definition List
  const ratioBarRow: React.ReactElement = (
    <React.Fragment key={`percent-${candidate}`}>
      <dt>Percent</dt>
      <dd>
        <RatioBar
          partyName={partyName}
          votes={candidateTotalVotes}
          totalVotes={totalVotesBiden + totalVotesTrump}
        />
      </dd>
    </React.Fragment>
  );

  return (
    <React.Fragment>
      {voteTypeRows}
      {voteSumRow}
      {ratioBarRow}
    </React.Fragment>
  );
}
