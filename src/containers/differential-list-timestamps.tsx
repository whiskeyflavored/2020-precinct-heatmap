import React, { useCallback, useContext, useMemo } from "react";
import cx from "classnames";
import { CurrentLocaleActionsContext } from "../stores/locale.store";
import {
  TimelineActionsContext,
  TimestampContext,
  TimestampActionsContext,
} from "../stores/timeline.store";
import { VotingDataContext } from "../stores/voting.store";
import {
  convertTimestampToLocalTime,
  extractDateStringFromTimestamp,
} from "../etc/leaflet-helpers";
import sharedStyles from "./details-panel.module.scss";
import styles from "./differential-list-timestamps.module.scss";
import { ActionType, DispatchActions } from "../stores/types";

export function TimestampDifferentialList() {
  const { currentTimestamp } = useContext(TimestampContext);

  const { votingDataTotalsForPrecinct } = useContext(VotingDataContext);

  const currentLocaleActionsDispatch = useContext(CurrentLocaleActionsContext);

  const timelineActionsDispatch = useContext(TimelineActionsContext);

  const timestampActionsDispatch = useContext(TimestampActionsContext);

  const handleTimestampChange = useCallback(
    function (event: React.MouseEvent) {
      const target: HTMLButtonElement = event.target as HTMLButtonElement;

      const { timestamp } = target.dataset;

      if (timestamp) {
        timelineActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            focusTransition: true,
          },
        } as ActionType.UpdateTimeline);

        timestampActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            currentTimestamp: timestamp,
          },
        } as ActionType.UpdateTimeline);

        currentLocaleActionsDispatch({
          type: DispatchActions.updateAvailableLocales,
          details: {
            currentCountyName: undefined,
            currentPrecinctId: undefined,
            currentPrecinctName: undefined,
          },
        });
      }
    },
    [
      currentLocaleActionsDispatch,
      timelineActionsDispatch,
      timestampActionsDispatch,
    ]
  );

  const handleTimestampSelect = useCallback(
    function (event: React.ChangeEvent<HTMLInputElement>) {
      const target = event.target;

      const { timestamp } = target.dataset;

      if (timestamp) {
        timelineActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            focusTransition: true,
          },
        } as ActionType.UpdateTimeline);

        timestampActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            currentTimestamp: timestamp,
          },
        } as ActionType.UpdateTimeline);
      }
    },
    [timelineActionsDispatch, timestampActionsDispatch]
  );

  const tableEntries: React.ReactNode = useMemo(
    function (): React.ReactNode {
      if (votingDataTotalsForPrecinct) {
        const { timeseries } = votingDataTotalsForPrecinct;

        return timeseries?.map(function ({ biden, timestamp, trump }: any) {
          const timestampDate: Date = convertTimestampToLocalTime(timestamp);

          const timestampFormatOptions = { month: "short" };

          const timestampFormattedString: string = `${extractDateStringFromTimestamp(
            timestampDate,
            timestampFormatOptions
          )} - ${timestampDate.toLocaleTimeString()}`;

          const largerVoteSum: number = biden > trump ? biden : trump;

          const lesserVoteSum: number = biden > trump ? trump : biden;

          const voteDifferential: number = largerVoteSum - lesserVoteSum;

          const majorityParty: string = biden > trump ? "dem" : "gop";

          const partyCx: string = cx(
            sharedStyles["party-marker"],
            sharedStyles[majorityParty]
          );

          const isCurrentTimestamp: boolean = timestamp === currentTimestamp;

          return (
            <tr key={timestamp}>
              <td className={styles["radio-cell"]}>
                <input
                  data-timestamp={timestamp}
                  type="radio"
                  checked={isCurrentTimestamp}
                  onChange={handleTimestampSelect}
                ></input>
              </td>
              <td className={sharedStyles["timestamp"]}>
                <span data-timestamp={timestamp}>
                  {timestampFormattedString}
                </span>
              </td>
              <td>
                {`+ ${voteDifferential}`}
                <div className={partyCx}></div>
              </td>
            </tr>
          );
        });
      }
    },
    [currentTimestamp, handleTimestampSelect, votingDataTotalsForPrecinct]
  );

  return (
    <table
      className={cx(sharedStyles["details-table"], styles["timestamp-table"])}
    >
      <thead>
        <tr>
          <th className={styles["radio-cell"]}>
            <input disabled type="radio" className={sharedStyles["ghost"]} />
          </th>
          <th>Timestamp</th>
          <th>Differential</th>
        </tr>
      </thead>
      <tbody>{tableEntries}</tbody>
    </table>
  );
}
