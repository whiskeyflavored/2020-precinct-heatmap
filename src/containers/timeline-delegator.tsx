import React, {
  useContext,
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
} from "react";
import { timelineSizes } from "../etc/constants";
// import helper methods
import { convertTimestampToLocalTime } from "../etc/leaflet-helpers";
//import { findVotingDataAtTimestamp, VotingDataAtTimestamp} from "../etc/voting-data-helpers";
// import API methods
import {
  getTimeStampsForPrecintInState,
  getTimestampsForState,
} from "../etc/api";
// import store contexts
import { ActionType, DispatchActions } from "../stores/types";
import { ApplicationActionsContext } from "../stores/application.store";
import { CurrentLocaleContext } from "../stores/locale.store";
import {
  TimelineContext,
  TimelineActionsContext,
  TimestampContext,
  TimestampActionsContext,
} from "../stores/timeline.store";

// how many milliseconds exist in a day, used for offsetting time markers
// in the ruler component.
const msInADay: number = 86400000;

export function TimelineDelegator({ children }: any) {
  // destructure application context level state
  const { currentStateAbbr, currentPrecinctName } = useContext(
    CurrentLocaleContext
  );

  // destructure timeline context level state
  const { timelineEndTime, timelineScale, timelineStartTime } = useContext(
    TimelineContext
  );

  // store values of current timestamp info
  const { currentTimestamp, timestampsForPrecinct } = useContext(
    TimestampContext
  );

  // Access to application-level action dispatcher
  const appActionsDispatch = useContext(ApplicationActionsContext);

  // Access to timeline-level action dispatcher
  const timelineActionsDispatch = useContext(TimelineActionsContext);

  // Access to timestamp-level action dispatcher
  const timestampActionsDispatch = useContext(TimestampActionsContext);

  // capture historically the last state to compare against context value of updated US state
  const previousUsState: React.MutableRefObject<string | undefined> = useRef();

  // A callback to fetch timestamps relevant to the current selected precinct within
  // a US state. Sets up a blocker prior to fetch, and sets the store after results
  // are collected.
  const fetchPrecinctTimestamps = useCallback(
    function () {
      (async function () {
        appActionsDispatch({
          type: DispatchActions.updateFetchStatus,
          details: {
            isFetching: true,
            process: "fetch precinct timestamps",
          },
        } as ActionType.UpdateFetchStatus);

        // Fetch the new timestamps associated with the precinct name
        const timestampsForPrecinct: string[] = await getTimeStampsForPrecintInState(
          currentStateAbbr,
          currentPrecinctName
        );

        // capture these timestamps in the timestamp store
        timestampActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            timestampsForPrecinct: timestampsForPrecinct,
          },
        } as ActionType.UpdateTimeline);

        appActionsDispatch({
          type: DispatchActions.updateFetchStatus,
          details: {
            isFetching: false,
            process: "fetch precinct timestamps",
          },
        } as ActionType.UpdateFetchStatus);
      })();
    },
    [
      appActionsDispatch,
      currentPrecinctName,
      currentStateAbbr,
      timestampActionsDispatch,
    ]
  );

  // Register a callback which will fetch a list of available timestamps for a
  // selected state when updated. Derive the beginning and endpoint dates to encompass
  // the entirety of the timeline. Capture all relevant information in our store.
  const fetchStateTimestamps = useCallback(
    function () {
      (async function fetchStateTimestamps() {
        appActionsDispatch({
          type: DispatchActions.updateFetchStatus,
          details: {
            isFetching: true,
            process: "fetch state timestamps",
          },
        } as ActionType.UpdateFetchStatus);

        // API call to fetch the available timestamps for the current US state
        const timestampsForState: string[] = await getTimestampsForState(
          currentStateAbbr
        );

        // We want the beginning of the day of which the first timestamp falls in
        const timelineStartTime: Date = new Date(
          convertTimestampToLocalTime(
            timestampsForState[0]
          ).toLocaleDateString()
        );

        // And similarly grab reference to the end of the day containing the last timestamp
        const timelineEndTime: Date = new Date(
          Date.parse(
            convertTimestampToLocalTime(
              timestampsForState[timestampsForState.length - 1]
            ).toLocaleDateString()
          ) + msInADay
        );

        // after the timestamps are fetched, capture them in the timeline store
        timelineActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            timelineEndTime,
            timelineStartTime,
          },
        } as ActionType.UpdateTimeline);

        // after the timestamps are fetched, capture them in the timeline store
        timestampActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            currentTimestamp: timestampsForState[0],
            timestampsForPrecinct: undefined,
            timestampsForState,
          },
        } as ActionType.UpdateTimeline);

        appActionsDispatch({
          type: DispatchActions.updateFetchStatus,
          details: {
            isFetching: false,
            process: "fetch state timestamps",
          },
        } as ActionType.UpdateFetchStatus);
      })();
    },
    [
      appActionsDispatch,
      currentStateAbbr,
      timestampActionsDispatch,
      timelineActionsDispatch,
    ]
  );

  // Manage statefulness of timestamps given change of dependent store values.
  // Fetches  initial round of timestamps and updates precinct level timestamps on
  // change of current precinct selection.
  useEffect(
    function updateTimestamps() {
      // conditional to there being a selected US state
      if (currentStateAbbr) {
        // Condition for when the selected precinct is updated.
        if (currentPrecinctName) {
          fetchPrecinctTimestamps();
        }

        // Case where the US state value changes, but no precinct is present in the store.
        // Trigger an update to clear any precinct-level timestamps in the store.
        else if (previousUsState.current !== currentStateAbbr) {
          previousUsState.current = currentStateAbbr;

          fetchStateTimestamps();
        }
      }
    },
    [
      appActionsDispatch,
      currentPrecinctName,
      currentStateAbbr,
      previousUsState,
      timestampActionsDispatch,
      fetchPrecinctTimestamps,
      fetchStateTimestamps,
    ]
  );

  // Given a change in precinct timestamps, determine whether the precinct currentTimestamp
  // is contained within that list of relevant timestamps. If it is not, reset the current
  // timestamp to be the first timestamp in the list.
  useEffect(
    function updateCurrentTimestamp() {
      if (timestampsForPrecinct && currentTimestamp) {
        // Find a matching index of the currentTimestamp value within the precinct timestamp list.
        // Returns -1 if no match found
        const matchingTimestampIndex: number = timestampsForPrecinct.indexOf(
          currentTimestamp as string
        );

        // Set the initial timestamp to 0 if the match number was negative (no match)
        const initialTimestampIndex: number =
          matchingTimestampIndex > 0 ? matchingTimestampIndex : 0;

        // set the transition state to true in the store
        timelineActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            focusTransition: true,
          },
        } as ActionType.UpdateTimeline);

        // capture these timestamps in the timestamp store
        timestampActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            currentTimestamp: timestampsForPrecinct[initialTimestampIndex],
          },
        } as ActionType.UpdateTimeline);
      }
    },
    [
      timestampsForPrecinct,
      currentTimestamp,
      timelineActionsDispatch,
      timestampActionsDispatch,
    ]
  );

  // When the timeline scale changes, update the store to reflect the latest timeline width.
  // Rule count is part of deriving this value, so set this value here as well.
  useLayoutEffect(
    function updateTimelineScale() {
      if (timelineEndTime && timelineStartTime) {
        // extract the amount of pixels to represent a unit of time on the timeline (hour)
        const { timeUnitSize } = timelineSizes;

        // determine the number of rules in the timeline based on the start and end times
        // in the timeline
        const timelineRuleCount: number = Math.ceil(
          (timelineEndTime.getTime() - timelineStartTime.getTime()) /
            (msInADay / 24)
        );

        // calculate the width of the timeline based on derived state values
        const timelineWidth = timelineRuleCount * timeUnitSize * timelineScale;

        // update the store to reflect changes to the timeline scale and rule count
        timelineActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            timelineRuleCount,
            timelineWidth,
          },
        } as ActionType.UpdateTimeline);
      }
    },
    [timelineScale, timelineActionsDispatch, timelineEndTime, timelineStartTime]
  );

  // Passively wrap whatever children are passed through the Delegator component
  return <React.Fragment>{children}</React.Fragment>;
}
