import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
} from "react";
import cx from "classnames";

import styles from "./timeline.module.scss";

import { ActionType, DispatchActions } from "../stores/types";

import {
  TimelineContext,
  TimelineActionsContext,
  TimestampActionsContext,
} from "../stores/timeline.store";

import { VotingDataContext } from "../stores/voting.store";

import {
  findVotingDataAtTimestamp,
  VotingDataAtTimestamp,
} from "../etc/voting-data-helpers";

import { timelineSizes } from "../etc/constants";

interface TimelineMarkersProps {
  timestampDictionary?: any;
}

export function TimelineMarkers({ timestampDictionary }: TimelineMarkersProps) {
  const { timelineWidth } = useContext(TimelineContext);

  const { votingDataTotalsForState } = useContext(VotingDataContext);

  // destructure reference to timeline context dispatcher
  const timelineActionsDispatch = useContext(TimelineActionsContext);

  // Reference to timestamp context dispatcher
  const timestampActionsDispatch = useContext(TimestampActionsContext);

  const svgRef: React.RefObject<SVGSVGElement> = useRef(null);

  //Interaction triggered events dealing with timeline manipulation and navigation
  const handleSelectTimestamp = useCallback(
    function (event: React.MouseEvent | React.FocusEvent) {
      // hold reference to the event target element
      const target: SVGLineElement = event.target as SVGLineElement;

      // extract the timestamp stored as a data attribute on the target
      const timestamp: string | null = target.getAttribute("data-timestamp");

      // determine whether element is disabled
      const isDisabled: boolean =
        target.getAttribute("data-disabled") === "true";

      // update the store to indicate we want the timeline to animate
      if (timestamp && !isDisabled) {
        timelineActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            focusTransition: true,
          },
        } as ActionType.UpdateTimeline);

        // update the store to reflect the selection of a new timestamp
        timestampActionsDispatch({
          type: DispatchActions.updateTimeline,
          details: {
            currentTimestamp: timestamp,
          },
        } as ActionType.UpdateTimeline);
      }
    },
    [timelineActionsDispatch, timestampActionsDispatch]
  );

  /* 
  To render the voter data within the timeline in a way that allows for each timestamp
  to be measured in relative size to the largest timestamp 'dump', we want to cache which
  timestamp in the data currently has the largest total votes. Memoize this value.
  */
  // Iterate through the voting data array and determine the largest vote total to use
  // as a matter of scale for rendering all ticks in the graph.
  const largestVoteTotal: number = useMemo(
    () =>
      votingDataTotalsForState?.timeseries.reduce(function (
        highestTotal: number,
        { biden = 0, trump = 0 }: any
      ) {
        const totalVotes: number = biden + trump;
        return highestTotal > totalVotes ? highestTotal : totalVotes;
      }),
    [votingDataTotalsForState]
  );

  // Memoize the timestamp markers as a series of SVG elements contained within a grouping.
  // The markers are utilized during rendering to represent timestamps located on the timeline.
  const timestampMarkers = useMemo(
    function renderTimestampMarkers() {
      // Copy the voting data totals for the state
      const votingDataTimeseries: any[] = votingDataTotalsForState
        ? Array.from(votingDataTotalsForState.timeseries)
        : [];

      const markerPositions: string[] = timestampDictionary
        ? Object.keys(timestampDictionary)
        : [];

      // Iterate through each timestamp and render markers and relevant voting data on the timeline
      return markerPositions?.map(
        (positionKey: string): React.ReactElement => {
          // Coerce the key into a number type
          const position: number = parseFloat(positionKey);

          // Refer to the timestamp  relative to this marker position
          const { timestamp } = timestampDictionary[positionKey];

          // Find any matching data for this timestamp. Destructure the retrun object to extract
          // voter data specific to each candidate.
          const {
            index,
            totalVotes,
            votesForBiden,
            votesForTrump,
          }: VotingDataAtTimestamp = findVotingDataAtTimestamp(
            timestamp,
            votingDataTimeseries
          );

          // remove the matched/found index from the timeseries duplicate array for quicker
          // iterations going forward.
          votingDataTimeseries.splice(index, 1);

          /*
          Calculate offsets and store them as variables to reference while constructing each
          vector component/segment to inject into the timeline. 
          */

          // define the top most point to render the voting data on the timeline
          const y_maxHeight: number =
            (totalVotes / largestVoteTotal) *
            (timelineSizes.trackHeight - timelineSizes.trackPadding * 2);

          // define the top rendering point of the GOP voting data segment
          const height_gop: number = votesForTrump
            ? (votesForTrump / totalVotes) * y_maxHeight
            : 0;

          // define the top rendering point of the DEM voting data segment
          const height_dem: number = votesForBiden
            ? (votesForBiden / totalVotes) * y_maxHeight
            : 0;

          // define the bottom most point to render the voting data on the timeline
          const votingData_y1: number =
            timelineSizes.trackHeight - timelineSizes.trackPadding;

          // calculate the mid-point between gop & dem voting data lines to start/end
          const votingData_y2: number = votingData_y1 - height_gop;

          // calculate the top-most point for the combined voting data vectors
          const votingData_y3: number = votingData_y1 - height_gop - height_dem;

          // define the start point for the marker to render on the timeline
          const marker_y1: number = timelineSizes.trackHeight;

          // define the ending point for the marker to render on the timeline
          const marker_y2: number =
            timelineSizes.trackHeight + timelineSizes.rulerHeight;

          /*
          Construct classnames to be appended to each vector segment. 
          */

          // creates a className string for this marker
          const markerCx: string = cx(styles["timestamp-marker"]);

          // define party-specific classes to apply to each segment of the voting data vectors
          const demCx: string = cx(styles["timeline-element"], styles["dem"]);
          const gopCx: string = cx(styles["timeline-element"], styles["gop"]);

          // DEM line segment
          const demLineSegment: React.ReactElement | null = height_dem ? (
            <line
              x1={position}
              y1={votingData_y2}
              x2={position}
              y2={votingData_y3}
              className={demCx}
            ></line>
          ) : null;

          // GOP line segment
          const gopLineSegment: React.ReactElement | null = height_gop ? (
            <line
              x1={position}
              y1={votingData_y1}
              x2={position}
              y2={votingData_y2}
              className={gopCx}
            ></line>
          ) : null;

          // Timestamp marker component
          const markerSegment: React.ReactElement = (
            <line
              x1={position}
              y1={marker_y1}
              x2={position}
              y2={marker_y2}
              className={markerCx}
            ></line>
          );

          // render the marker segments
          return (
            <React.Fragment key={`timestamp-segment-${timestamp}`}>
              <g
                data-timestamp={timestamp}
                className={styles["timeline-element-group"]}
                tabIndex={0}
                onMouseUp={handleSelectTimestamp}
                onFocus={handleSelectTimestamp}
              >
                {demLineSegment}
                {gopLineSegment}
                {markerSegment}
              </g>
            </React.Fragment>
          );
        }
      );
    },
    [
      handleSelectTimestamp,
      largestVoteTotal,
      timestampDictionary,
      votingDataTotalsForState,
    ]
  );

  useEffect(
    function resizeSvg() {
      if (svgRef.current && timelineWidth) {
        svgRef.current.setAttribute("width", timelineWidth);
      }
    },
    [svgRef, timelineWidth]
  );

  return (
    <svg ref={svgRef} y={timelineSizes.rulerHeight}>
      {timestampMarkers}
    </svg>
  );
}
