import React, { useContext, useEffect } from "react";
import { Chloropleth } from "./chloropleth";
import { ActionType, DispatchActions } from "../stores/types";
import { ApplicationActionsContext } from "../stores/application.store";
import { TimestampContext } from "../stores/timeline.store";
import {
  VotingDataContext,
  VotingDataActionsContext,
} from "../stores/voting.store";
import { CurrentLocaleContext } from "../stores/locale.store";
import { MapActionsContext } from "../stores/map.store";
import {
  getPrecinctGeographyDataForState,
  getVoteTotalsForPrecinct,
  getVoteTotalsForState,
  getVotingDataAtTimestampForStateByType,
} from "../etc/api";

export function ChloroplethDelegator() {
  const appActionsDispatch = useContext(ApplicationActionsContext);

  const {
    currentCountyName,
    currentStateAbbr,
    currentPrecinctName,
  } = useContext(CurrentLocaleContext);

  const { currentTimestamp } = useContext(TimestampContext);

  const { votingDataForStateAtTimestamp } = useContext(VotingDataContext);

  const mapContextDispatch = useContext(MapActionsContext);

  const votingDataActionsDispatch = useContext(VotingDataActionsContext);

  // initially fetch data to render in the map view
  useEffect(
    function fetchInitialGeographyData() {
      if (currentStateAbbr) {
        (async function () {
          // set fetch state to true
          appActionsDispatch({
            type: DispatchActions.updateFetchStatus,
            details: { isFetching: true, process: "fetch geoJson" },
          } as ActionType.UpdateFetchStatus);

          // fetch all precinct-level geoJson data for this state
          const geoJsonData: any = await getPrecinctGeographyDataForState(
            currentStateAbbr
          );

          // commit geoJson to application store
          mapContextDispatch({
            type: DispatchActions.updateGeographyData,
            details: {
              geoJsonPrecincts: geoJsonData.details,
            },
          } as ActionType.UpdateGeographyData);
        })();
      }
    },
    [appActionsDispatch, currentStateAbbr, mapContextDispatch]
  );

  // Given an existing set of voting data, and a defined precinct and county name
  // as arguments, search through the voting data to extract the proper item reference.
  // Once identified, capture this precinct-level voting data in state.
  useEffect(
    function updatePrecinctVotingDataAtTimestamp() {
      if (
        currentPrecinctName &&
        currentCountyName &&
        votingDataForStateAtTimestamp
      ) {
        for (let dataItem of votingDataForStateAtTimestamp) {
          // test for match of both precinct and county names
          if (
            dataItem.precinct_id === currentPrecinctName &&
            dataItem.locality_name === currentCountyName
          ) {
            // Capture the matching precinct data into the store
            votingDataActionsDispatch({
              type: DispatchActions.updateVotingData,
              details: {
                votingDataForPrecinctAtTimestamp: dataItem,
              },
            } as ActionType.UpdateVotingData);

            break;
          }
        }
      }
    },
    [
      appActionsDispatch,
      currentCountyName,
      currentPrecinctName,
      votingDataActionsDispatch,
      votingDataForStateAtTimestamp,
    ]
  );

  useEffect(
    function getPrecinctVotingTotals() {
      if (currentStateAbbr && currentCountyName && currentPrecinctName) {
        getVoteTotalsForPrecinct(
          currentStateAbbr,
          currentCountyName,
          currentPrecinctName
        ).then(function (precinctTotals: any) {
          votingDataActionsDispatch({
            type: DispatchActions.updateVotingData,
            details: {
              votingDataTotalsForPrecinct: precinctTotals,
            },
          });
        });
      }
    },
    [
      currentCountyName,
      currentPrecinctName,
      currentStateAbbr,
      votingDataActionsDispatch,
    ]
  );

  // Update state voting totals on change of state and put it in the store
  useEffect(
    function getStateVotingTotals() {
      if (currentStateAbbr) {
        getVoteTotalsForState(currentStateAbbr).then(function updateVotingStore(
          votingTotals: any
        ) {
          votingDataActionsDispatch({
            type: DispatchActions.updateVotingData,
            details: {
              votingDataTotalsForState: votingTotals,
            },
          } as ActionType.UpdateVotingData);
        });
      }
    },
    [currentStateAbbr, votingDataActionsDispatch]
  );

  useEffect(
    function updateStateVotingDataAtTimestamp() {
      if (currentStateAbbr && currentTimestamp) {
        // set fetch state to true
        appActionsDispatch({
          type: DispatchActions.updateFetchStatus,
          details: { isFetching: true, process: "render map" },
        } as ActionType.UpdateFetchStatus);

        // given a state and a timestamp, fetch the voting data relevant to these arguments
        getVotingDataAtTimestampForStateByType(
          currentStateAbbr,
          currentTimestamp
        ).then(function resolveAPIFetch(stateVotingDataAtTimestamp) {
          votingDataActionsDispatch({
            type: DispatchActions.updateVotingData,
            details: {
              votingDataForStateAtTimestamp: stateVotingDataAtTimestamp,
            },
          } as ActionType.UpdateVotingData);
        });
      }
    },
    [
      appActionsDispatch,
      currentStateAbbr,
      currentTimestamp,
      votingDataActionsDispatch,
    ]
  );

  return <Chloropleth />;
}
