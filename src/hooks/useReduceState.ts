import { DispatchAction, DispatchActions } from "../stores/types";

export function useReduceState(currentState: any, action: DispatchAction) {
  switch (action.type) {
    case DispatchActions.updateFetchStatus:
      // destructure details from the action dispatcher
      const { isFetching, process } = action.details;

      // A list of application-blocking processes currently registered in the store
      const { processList } = currentState;

      // convert process to an array
      const useProcesses: unknown[] =
        process instanceof Array ? process : [process];

      // condition to add fetch state
      if (isFetching) {
        const processListMixin: unknown[] = useProcesses
          .map(function (addProcess: unknown) {
            return processList.includes(addProcess) ? undefined : addProcess;
          })
          .filter((n) => n);

        // dont update the processList list if this is a duplicate
        const newProcessList: unknown[] = [...processList, ...processListMixin];

        // store the new state
        return { isFetching, processList: newProcessList };
      }

      // condition to remove fetch state
      else {
        // create a new callee list from the original, and remove any matched in the new list
        const newProcessList: unknown[] = Array.from(processList).filter(
          (entry: unknown) => !useProcesses.includes(entry)
        );

        // set a new fetch status, which only will set to false if all processList have been
        // removed from the list.
        const newFetchStatus: boolean = Boolean(newProcessList.length);

        // store the new state
        return {
          isFetching: newFetchStatus,
          processList: newProcessList,
        };
      }

    // update the available states supported by the back-end
    case DispatchActions.updateAvailableLocales:
      return Object.assign({}, currentState, action.details);

    case DispatchActions.updateCurrentLocale:
      const {
        currentCountyName,
        currentCountyId,
        currentStateAbbr,
        currentPrecinctId,
        currentPrecinctName,
      } = action.details;

      // detect whether the state is asking to be updated
      const didUsStateChange: boolean = Boolean(
        action.details.hasOwnProperty("currentStateAbbr") &&
          currentState.currentStateAbbr !== currentStateAbbr
      );

      // detect whether the county is asking to be updated
      const didCountyChange: boolean = Boolean(
        action.details.hasOwnProperty("currentCountyName") &&
          currentState.currentCountyName !== currentCountyName
      );

      // determine the new state abbr value to be set in the store
      const newStateAbbr: string = action.details.hasOwnProperty(
        "currentStateAbbr"
      )
        ? currentStateAbbr
        : currentState.currentStateAbbr;

      // determine the new county details to be set in the store. This will be cleared
      // if a new state value request was detected.
      const newCountyDetails: object = didUsStateChange
        ? { currentCountyId: undefined, currentCountyName: undefined }
        : {
            currentCountyId: action.details.hasOwnProperty("currentCountyId")
              ? currentCountyId
              : currentState.currentCountyId,
            currentCountyName: action.details.hasOwnProperty(
              "currentCountyName"
            )
              ? currentCountyName
              : currentState.currentCountyName,
          };

      // conditionally set new precinct level details to set in the store. This will be
      // cleared if either the county or state level change request was detected.
      const newPrecinctDetailsPanel: object =
        didUsStateChange ||
        (didCountyChange && !(currentPrecinctId && currentPrecinctName))
          ? { currentPrecinctId: undefined, currentPrecinctName: undefined }
          : {
              currentPrecinctId: action.details.hasOwnProperty(
                "currentPrecinctId"
              )
                ? currentPrecinctId
                : currentState.currentPrecinctId,
              currentPrecinctName: action.details.hasOwnProperty(
                "currentPrecinctName"
              )
                ? currentPrecinctName
                : currentState.currentPrecinctName,
            };

      // store the new derived state
      return Object.assign(
        {},
        currentState,
        { currentStateAbbr: newStateAbbr },
        newCountyDetails,
        newPrecinctDetailsPanel
      );

    default:
      throw new Error(
        `Dispatched event '${action.type}' had no state management handler`
      );
  }
}

export function useReduceMapPopoverState(
  currentState: any,
  action: DispatchAction
) {
  switch (action.type) {
    case DispatchActions.updateGeographyData:
      return Object.assign({}, currentState, action.details);

    case DispatchActions.updateMapState:
      return Object.assign({}, currentState, action.details);

    default:
      throw new Error(
        `Dispatched event '${action.type}' had no state management handler`
      );
  }
}

export function useReduceTimelineState(
  currentState: any,
  action: DispatchAction
) {
  switch (action.type) {
    // update the current selected Timestamp info
    case DispatchActions.updateTimeline:
      return Object.assign({}, currentState, action.details);

    default:
      throw new Error(
        `Dispatched event '${action.type}' had no state management handler`
      );
  }
}

export function useReduceVotingDataState(
  currentState: any,
  action: DispatchAction
) {
  switch (action.type) {
    // voting data store updates
    case DispatchActions.updateVotingData:
      return Object.assign({}, currentState, action.details);

    default:
      throw new Error(
        `Dispatched event '${action.type}' had no state management handler`
      );
  }
}
