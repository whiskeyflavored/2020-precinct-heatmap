// define enumerated set of dispatch actions to be handled within the reducer hook
export enum DispatchActions {
  "updateFetchStatus" = "updateFetchStatus",
  "updateAvailableLocales" = "updateAvailableLocales",
  "updateCurrentLocale" = "updateCurrentLocale",
  "updateGeographyData" = "updateGeographyData",
  "updateMapState" = "updateMapState",
  "updateTimeline" = "updateTimeline",
  "updateVotingData" = "updateVotingData",
}

declare namespace ActionTypeDetails {
  export interface UpdateFetchStatus {
    isFetching: boolean;
    process: unknown;
  }
  export interface UpdateAvailableLocales {
    availableCounties?: any;
    availablePrecincts?: any;
    availableStates: string[];
  }
  export interface UpdateCurrentLocale {
    currentCountyId?: string;
    currentCountyName?: string;
    currentPrecinctId?: string;
    currentPrecinctName?: string;
    currentStateAbbr?: string;
  }
  export interface UpdateGeographyData {
    geoJosnCounties?: any;
    geoJsonPrecincts?: any;
  }
  export interface UpdateMapState {
    isMapPopoverVisible?: boolean;
    mapPopoverCountyName?: string;
    mapPopoverPositionX?: number;
    mapPopoverPositionY?: number;
    mapPopoverPrecinctName?: string;
  }
  export interface UpdateTimeline {
    currentTimestamp?: string;
    currentTimestampPositionX?: number;
    focusTransition?: boolean;
    timelineEndTime?: Date;
    timelineHasInteraction?: boolean;
    timelineInteractionInitialX?: number;
    timelinePanX?: number;
    timelineRuleCount?: number;
    timelineScale: number;
    timelineStartTime?: Date;
    timelineWidth?: number;
    timestampsForState?: string[];
    timestampsForPrecinct?: string[];
    zoomMin: number;
    zoomMax: number;
  }
  export interface UpdateVotingData {
    availableVoteTypes?: string[];
    votingDataForPrecinctAtTimestamp?: any;
    votingDataForStateAtTimestamp?: any;
    votingDataTotalsForPrecinct?: any;
    votingDataTotalsForState?: any;
  }
}

export declare namespace ActionType {
  export interface UpdateFetchStatus {
    type: DispatchActions.updateFetchStatus;
    details: ActionTypeDetails.UpdateFetchStatus;
  }
  export interface UpdateAvailableLocales {
    type: DispatchActions.updateAvailableLocales;
    details: ActionTypeDetails.UpdateAvailableLocales;
  }
  export interface UpdateCurrentLocale {
    type: DispatchActions.updateCurrentLocale;
    details: ActionTypeDetails.UpdateCurrentLocale;
  }
  export interface UpdateGeographyData {
    type: DispatchActions.updateGeographyData;
    details: ActionTypeDetails.UpdateGeographyData;
  }
  export interface UpdateMapState {
    type: DispatchActions.updateMapState;
    details: ActionTypeDetails.UpdateMapState;
  }
  export interface UpdateTimeline {
    type: DispatchActions.updateTimeline;
    details: ActionTypeDetails.UpdateTimeline;
  }
  export interface UpdateVotingData {
    type: DispatchActions.updateVotingData;
    details: ActionTypeDetails.UpdateVotingData;
  }
}

// Define the type to expect as a payload for a dispatch call
export type DispatchAction =
  | ActionType.UpdateFetchStatus
  | ActionType.UpdateAvailableLocales
  | ActionType.UpdateCurrentLocale
  | ActionType.UpdateGeographyData
  | ActionType.UpdateMapState
  | ActionType.UpdateTimeline
  | ActionType.UpdateVotingData;
