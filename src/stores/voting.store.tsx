import React, { createContext, useReducer } from "react";
import { useReduceVotingDataState } from "../hooks/useReduceState";

type votingDataStore = {
  votingDataForPrecinctAtTimestamp?: any;
  votingDataForStateAtTimestamp?: any;
  votingDataTotalsForPrecinct?: any;
  votingDataTotalsForState?: any;
};

// define an initial state to populate the store
const votingDataInitialState: votingDataStore = {};

export const VotingDataContext: React.Context<votingDataStore> = createContext(
  votingDataInitialState
);

export const VotingDataActionsContext: React.Context<any> = createContext({});

export function VotingDataProvider({ children }: any) {
  const [votingDataState, votingDataDispatch] = useReducer(
    useReduceVotingDataState,
    votingDataInitialState
  );

  return (
    <VotingDataContext.Provider value={votingDataState}>
      <VotingDataActionsContext.Provider value={votingDataDispatch}>
        {children}
      </VotingDataActionsContext.Provider>
    </VotingDataContext.Provider>
  );
}
