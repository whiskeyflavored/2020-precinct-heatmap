import React, { createContext, useReducer } from "react";
import { useReduceTimelineState } from "../hooks/useReduceState";
import {timelineSizes} from "../etc/constants";

type TimelineStore = {
  focusTransition?: boolean;
  timelineEndTime?: Date;
  timelineHasInteraction?: boolean;
  timelineInteractionInitialX?: number;
  timelinePanX?: number;
  timelineRuleCount?: number;
  timelineScale: number;
  timelineStartTime?: Date;
  timelineWidth?: number;
  zoomMin: number;
  zoomMax: number;
};

type TimestampStore = {
  currentTimestamp?: string;
  currentTimestampPositionX?: number;
  timestampsForPrecinct?: string[];
  timestampsForState?: string[];
};

// define initial states to populate each store
const initialStateTimeline: TimelineStore = {
  timelineHasInteraction: false,
  timelinePanX: -timelineSizes.overscrollThreshold,
  timelineScale: 1,
  zoomMin: 1,
  zoomMax: 5,
};

const initialStateTimestamp: TimestampStore = {};

// Create context instances for each store, broken down by the state and dispatch(actions)
// components of each context.
export const TimelineContext: React.Context<any> = createContext(
  initialStateTimeline
);

export const TimestampContext: React.Context<TimestampStore> = createContext(
  initialStateTimestamp
);

export const TimelineActionsContext: React.Context<any> = createContext({});

export const TimestampActionsContext: React.Context<any> = createContext({});

// Export a component which will wrap a react element to expose the values of each
// context instance
export function TimelineProvider({ children }: any) {
  const [timelineState, timelineDispatch] = useReducer(
    useReduceTimelineState,
    initialStateTimeline
  );
  const [timestampState, timestampDispatch] = useReducer(
    useReduceTimelineState,
    initialStateTimestamp
  );

  return (
    <TimelineContext.Provider value={timelineState}>
      <TimelineActionsContext.Provider value={timelineDispatch}>
        <TimestampContext.Provider value={timestampState}>
          <TimestampActionsContext.Provider value={timestampDispatch}>
            {children}
          </TimestampActionsContext.Provider>
        </TimestampContext.Provider>
      </TimelineActionsContext.Provider>
    </TimelineContext.Provider>
  );
}
