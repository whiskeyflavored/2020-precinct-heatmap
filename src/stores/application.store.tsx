import React, { createContext, useReducer } from "react";
import { useReduceState } from "../hooks/useReduceState";

// define the shape of the store used throughout the application
type ApplicationStore = {
  isFetching: boolean;
  processList: unknown[];
};

// define an initial state to populate the store
const initialState: ApplicationStore = {
  isFetching: false,
  processList: [],
};

// Holds the application state in an object. Creates a Context instance.
export const ApplicationContext: React.Context<ApplicationStore> = createContext(
  initialState
);

// Hold actions context of application state
export const ApplicationActionsContext: React.Context<any> = createContext({});

// Expose a wrapper to pass through application state
export function StateProvider({ children }: any) {
  const [state, dispatch] = useReducer(useReduceState, initialState);

  return (
    <ApplicationContext.Provider value={state}>
      <ApplicationActionsContext.Provider value={dispatch}>
        {children}
      </ApplicationActionsContext.Provider>
    </ApplicationContext.Provider>
  );
}
