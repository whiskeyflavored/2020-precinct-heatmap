import React, { createContext, useReducer } from "react";
import { StateInformation, geographicConstants } from "../etc/geographic-data";
import { useReduceState } from "../hooks/useReduceState";

type AvailableLocaleStore = {
  availableCounties?: any[];
  availablePrecincts?: any[];
  availableStates?: string[];
  availableVoteTypes?: string[];
  // local data which keeps information about state-level details
  // necessary for proper map rendering and other useful values
  stateLevelInfo: StateInformation;
};

type CurrentLocaleStore = {
  // state of current goegraphic locality selections
  currentCountyId?: string;
  currentCountyName?: string;
  currentPrecinctId?: string;
  currentPrecinctName?: string;
  currentStateAbbr?: string;
};

// define an initial state to populate the store
const availableLocaleInitialState: AvailableLocaleStore = {
  stateLevelInfo: geographicConstants,
};

const currentLocaleInitialState: CurrentLocaleStore = {
  currentStateAbbr: "GA",
};

// Holds the application state in an object. Creates a Context instance.
export const AvailableLocalesContext: React.Context<any> = createContext(
  availableLocaleInitialState
);

export const AvailableLocalesActionsContext: React.Context<any> = createContext(
  {}
);

// Context instances for current locale information
export const CurrentLocaleContext: React.Context<any> = createContext(
  currentLocaleInitialState
);

export const CurrentLocaleActionsContext: React.Context<any> = createContext(
  {}
);

// Expose a wrapper to pass through application state
export function LocaleProvider({ children }: any) {
  const [availableLocalesState, availableLocalesDispatch] = useReducer(
    useReduceState,
    availableLocaleInitialState
  );

  const [currentLocaleState, currentLocaleDispatch] = useReducer(
    useReduceState,
    currentLocaleInitialState
  );
	
  return (
    <AvailableLocalesContext.Provider value={availableLocalesState}>
      <AvailableLocalesActionsContext.Provider value={availableLocalesDispatch}>
        <CurrentLocaleContext.Provider value={currentLocaleState}>
          <CurrentLocaleActionsContext.Provider value={currentLocaleDispatch}>
            {children}
          </CurrentLocaleActionsContext.Provider>
        </CurrentLocaleContext.Provider>
      </AvailableLocalesActionsContext.Provider>
    </AvailableLocalesContext.Provider>
  );
}
