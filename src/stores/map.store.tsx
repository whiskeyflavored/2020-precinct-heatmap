import React, { createContext, useReducer } from "react";
import { useReduceMapPopoverState } from "../hooks/useReduceState";

type MapStore = {
  // geography related data;
  geoJsonPrecincts?: any;
  geoJsonCounties?: any;
  geoJsonStates?: any;
};

type MapPopoverStore = {
  // map popover properties
  isMapPopoverVisible?: boolean;
  mapPopoverCountyName?: string;
  mapPopoverPositionX?: number;
  mapPopoverPositionY?: number;
  mapPopoverPrecinctName?: string;
};

// define an initial state to populate the store
const initialMapPopoverStore: MapPopoverStore = {
  isMapPopoverVisible: false,
};

export const MapContext: React.Context<MapStore> = createContext({});

export const MapActionsContext: React.Context<any> = createContext({});

export const MapPopoverContext: React.Context<MapPopoverStore> = createContext(
  initialMapPopoverStore
);

export const MapPopoverActionsContext: React.Context<any> = createContext(
  {}
);

export function MapContextProvider({ children }: any) {
  const [mapState, mapDispatch] = useReducer(useReduceMapPopoverState, {});

  const [mapPopoverState, mapPopoverDispatch] = useReducer(
    useReduceMapPopoverState,
    initialMapPopoverStore
  );

  return (
    <MapContext.Provider value={mapState}>
      <MapActionsContext.Provider value={mapDispatch}>
        <MapPopoverContext.Provider value={mapPopoverState}>
          <MapPopoverActionsContext.Provider value={mapPopoverDispatch}>
            {children}
          </MapPopoverActionsContext.Provider>
        </MapPopoverContext.Provider>
      </MapActionsContext.Provider>
    </MapContext.Provider>
  );
}
