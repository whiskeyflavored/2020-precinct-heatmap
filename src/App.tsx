import React, { useContext, useEffect } from "react";
import cx from "classnames";
// import container components
import { Icon } from "ts-react-feather-icons";
import { ChloroplethDelegator } from "./containers/chloropleth-delegator";
import { TimelineDelegator } from "./containers/timeline-delegator";
import { Timeline } from "./containers/timeline";
import { AppHeader } from "./containers/app-header";
import { LoadIndicator } from "./components/load-indicator";
import { LocaleSelectors } from "./containers/locale-selectors";
import { TimestampDetailsPanel } from "./containers/details-for-timestamp";
import { PrecinctDetailsPanelLayer } from "./containers/details-for-precinct";
import { TimelineControlBar } from "./containers/timeline-controls";
// import contexts
import { ActionType, DispatchActions } from "./stores/types";
import {
  ApplicationContext,
  ApplicationActionsContext,
} from "./stores/application.store";
import {
  AvailableLocalesActionsContext,
  CurrentLocaleContext,
  CurrentLocaleActionsContext,
} from "./stores/locale.store";
import { VotingDataActionsContext } from "./stores/voting.store";
// import API methods
import {
  getAvailableCountiesInState,
  getAvailableStates,
  getAvailablePrecinctsForState,
  getVoteTypesForState,
} from "./etc/api";
// import styles
import styles from "./App.module.scss";
import "leaflet/dist/leaflet.css";
import "./App.scss";

// Main App component
export default function App() {
  // store value to determine if the application is currently fetching info from back-end systems
  const { isFetching } = useContext(ApplicationContext);

  // store values of any currently selected localities
  const { currentStateAbbr, currentPrecinctName } = useContext(
    CurrentLocaleContext
  );

  // store value of the current timestamp in focus
  // const { currentTimestamp } = useContext(TimestampContext);

  // dispatch app-level actions to modify store
  const appActionsDispatch = useContext(ApplicationActionsContext);

  // dispatch locale context level actions to modify locality store
  const availableLocalesDispatch = useContext(AvailableLocalesActionsContext);

  // dispatche current locale context actions
  const currentLocaleActionsDispatch = useContext(CurrentLocaleActionsContext);

  // dispatch voting data context to modify voter data store
  const votingDataContextDispatch = useContext(VotingDataActionsContext);

  // combined style classes for loader container. Dims the overlay when the
  // app is not in a current fetch state.
  const loaderCx: string = cx(styles["load-indicator-wrapper"], {
    [styles.dim]: !isFetching,
  });

  // App state initialization which fetches the localities that are available
  // and supported by the back-end. Throw up an interaction whall while
  // subsequent/dependent information is fetched.
  useEffect(
    function fetchInitialLocaleInfo() {
      (async function () {
        // Set fetch status indicating app is busy initializing
        appActionsDispatch({
          type: DispatchActions.updateFetchStatus,
          details: {
            isFetching: true,
            process: "init app",
          },
        } as ActionType.UpdateFetchStatus);

        // Grab all states that are available from the back-end
        const availableStates: any = await getAvailableStates();

        // update application store to include the results of available states
        availableLocalesDispatch({
          type: DispatchActions.updateAvailableLocales,
          details: {
            availableStates: availableStates,
          },
        } as ActionType.UpdateAvailableLocales);

        // bblock the UI as the app is initialized
        appActionsDispatch({
          type: DispatchActions.updateFetchStatus,
          details: {
            isFetching: false,
            process: "init app",
          },
        } as ActionType.UpdateFetchStatus);
      })();
    },
    [appActionsDispatch, availableLocalesDispatch]
  );

  // Get a list of geographic information relative to the current state.
  // Updates when 'currentStateAbbr' property has changed.
  useEffect(
    function captureAvailablesInStore() {
      if (currentStateAbbr) {
        (async function () {
          // Set fetch status indicating app is busy initializing
          appActionsDispatch({
            type: DispatchActions.updateFetchStatus,
            details: {
              isFetching: true,
              process: "fetch state data",
            },
          } as ActionType.UpdateFetchStatus);

          // begin async fetch of available counties in this current US state
          const availableCounties: Promise<any> = getAvailableCountiesInState(
            currentStateAbbr
          );
          // begin async fetch of available precincts for the current US state
          const availablePrecincts: Promise<any> = getAvailablePrecinctsForState(
            currentStateAbbr
          );

          // Begin async fetch of available vote types for the current state
          const availableVoteTypes: Promise<string[]> = getVoteTypesForState(
            currentStateAbbr
          );

          // Once all promises have resolved, dispatch appropraite actions to update the store.
          Promise.all([
            availableCounties,
            availablePrecincts,
            availableVoteTypes,
          ]).then(function ([
            resolvedCounties,
            resolvedPrecincts,
            resolvedVoteTypes,
          ]) {
            // Set fetch status indicating app is busy initializing
            appActionsDispatch({
              type: DispatchActions.updateFetchStatus,
              details: {
                isFetching: false,
                process: "fetch state data",
              },
            } as ActionType.UpdateFetchStatus);

            // update the store to reflect this current state's available precincts
            availableLocalesDispatch({
              type: DispatchActions.updateAvailableLocales,
              details: {
                availableCounties: resolvedCounties,
                availablePrecincts: resolvedPrecincts,
              },
            } as ActionType.UpdateAvailableLocales);

            // update any existing localities to be cleared
            currentLocaleActionsDispatch({
              type: DispatchActions.updateCurrentLocale,
              details: {
                currentPrecinctId: undefined,
                currentPrecinctName: undefined,
              },
            } as ActionType.UpdateCurrentLocale);

            // update the store to reflect available vote types supported by this state's voter data
            votingDataContextDispatch({
              type: DispatchActions.updateVotingData,
              details: {
                availableVoteTypes: resolvedVoteTypes,
              },
            } as ActionType.UpdateVotingData);
          });
        })();
      }
    },
    [
      appActionsDispatch,
      availableLocalesDispatch,
      votingDataContextDispatch,
      currentStateAbbr,
      currentLocaleActionsDispatch,
    ]
  );

  // rendered elements
  return (
    <React.Fragment>
      <div className={styles.page}>
        <header className={styles["app-header"]}>
          <AppHeader>
            <LocaleSelectors />
          </AppHeader>
        </header>
        <nav className={styles["view-mode-controls"]}>
          <LocaleSelectors />
          <div>
            <button>
              <Icon name="list" size={20} />
            </button>
            <button>
              <Icon name="map" size={20} />
            </button>
          </div>
        </nav>
        <aside className={styles["details-panel"]}>
          {currentPrecinctName ? (
            <PrecinctDetailsPanelLayer />
          ) : (
            <TimestampDetailsPanel />
          )}
        </aside>
        {/*
            Data Visualization panel:
            View a map with integrated timeline representing all relevant
            data entries at a given timeframe that can be applied to the map.
          */}
        <section className={styles["map-panel"]}>
          <ChloroplethDelegator />
        </section>
        <section className={styles["timeline-panel"]}>
          <TimelineDelegator>
            <Timeline />
          </TimelineDelegator>
        </section>
        <section className={styles["timeline-controls"]}>
          <TimelineControlBar />
        </section>
      </div>
      <div className={loaderCx}>
        <LoadIndicator className={styles["load-indicator"]} />
      </div>
    </React.Fragment>
  );
}
