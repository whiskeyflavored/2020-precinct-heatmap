import { chloroplethThresholds } from "../etc/constants";
import { deriveAggregatePartyVotesByType } from "./voting-data-helpers";

// looks for any leaflet-interactive class features which may have other
// selectors applied to them, and removes them from the classlist.
// These are static CSS classes that can be defined globally as it is how
// Leaflet CSS injection is handled (not modular)
export function clearInteractiveLayerStyling(classSelector: string) {
  document
    .querySelector(`.leaflet-interactive.${classSelector}`)
    ?.classList.remove(classSelector);
}

// Accepts a leaflet map element and clears any extra classes applied to the individual shapes
// within the map. These classes are attached to a data attribute during application of those classes
export function clearShapeStyling(leafletMapElement: Element): void {
  // define the base class that exists on an interactive feature within Leaflet.
  const baseFeatureClass: string = "leaflet-interactive";

  // which attribute to search for to extract applied classnames to this layer
  const dataClassAttributeName: string = "data-classes";

  const shapes: NodeListOf<Element> = leafletMapElement.querySelectorAll(
    `.${baseFeatureClass}`
  );

  // iterate through the collection of shapes
  Array.prototype.slice.call(shapes).forEach((element: SVGElement) => {
    // strip out the list of classes that were applied to this feature.
    // this is stored in a data attribute during rendering
    const appliedClasses: string[] | undefined = element
      .getAttribute(dataClassAttributeName)
      ?.split(",");

    // when not empty, remove each class from the layer
    if (appliedClasses) {
      appliedClasses.forEach((className: string) => {
        element.classList.remove(className);
      });
    }
  });
}

// Method to add 'color' to a given feature/layer representing a locality on the map.
// Colorizing is based on voter data supplied for this feature at a given timestamp.
export function colorizeMapFeature(votingData: any, mapLayer: any) {
  const { demVotes, gopVotes } = deriveAggregatePartyVotesByType(votingData.results);

  // calculate the absolute value between party votes
  const differential: number = Math.abs(demVotes - gopVotes);

  // grab reference to the underlying element of the Leaflet layer
  const svgElement: SVGElement = mapLayer._path;

  // reference to the parent layer
  const parentElement: (Node & ParentNode) | null = svgElement.parentNode;

  // determine majority party classname to apply to layer
  const partyCx: string =
    demVotes > gopVotes
      ? "majority-dem"
      : demVotes < gopVotes
      ? "majority-gop"
      : "no-majority";

  // determine the strength/scale classname to apply to layer
  const scaleCx: string = chloroplethThresholds.filter(
    ({ minValue, maxValue }: any) =>
      differential >= minValue && differential <= maxValue
  )[0]?.className;

  // apply a class to indicate the majority share  of votes by party
  svgElement.classList.add(partyCx, scaleCx);

  // assign applied classnames as a data attribute
  // this will assist in removing these state-specific classes if necessary
  svgElement.setAttribute("data-classes", `${partyCx},${scaleCx}`);

  // remove from the map layer and re-add it. This should push the layer to the top of
  // the rendering order, which is not configurable via styling for SVG components
  parentElement?.appendChild(svgElement);
}

// given a timestamp date, will convert to javascript Date instance as refernced
// in GMT. WIthout this specification, timestamps are parsed based on the users local machine time.
export function convertTimestampToLocalTime(
  timestamp: string | undefined
): Date {
  try {
    const convertedDate: Date = new Date(
      `${timestamp?.replace(/ /g, "T")}-00:00`
    );
    return convertedDate;
  } catch (error) {
    throw error;
  }
}

export function extractTimeStringFromTimestamp(timestamp: string) {
  const date: Date = convertTimestampToLocalTime(timestamp);
  return date.toLocaleTimeString();
  // return timestamp.split(" ")[1] || "";
}

export function extractDateStringFromTimestamp(
  date: Date,
  options: any = {}
): string {
  const { month = "short", timeZone = "UTC" } = options;

  return `${date.toLocaleString("default", {
    month,
    timeZone,
  })} ${date.getDate()}, ${date.getFullYear()}`;
}
