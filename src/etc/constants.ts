// keep a dictionary of classnames which dictate styling to be applied.
// This is done to help differentiate the 'weight' of the impact on how
// many votes were cast
export const chloroplethThresholds: any = [
  { minValue: 0, maxValue: 9, className: "nominal" },
  { minValue: 10, maxValue: 99, className: "slight" },
  { minValue: 100, maxValue: 499, className: "minor" },
  { minValue: 500, maxValue: 999, className: "moderate" },
  { minValue: 1000, maxValue: 1999, className: "significant" },
  { minValue: 2000, maxValue: Infinity, className: "extreme" },
];

// constant to hold measurement values used to render the timeline
export const timelineSizes: any = {
  overscrollThreshold: 120,
  rulerHeight: 16,
  rulerLabelSize: 10,
  trackHeight: 60,
  trackPadding: 8,
  timeUnitSize: 8,
  minScale: 1,
  maxScale: 5
};