// Given a segment of data from voting results returned via API, cycle through
// the results and return an object holding the aggregate value of each vote tally by party.
// Optionally supply a list of voteTypes th focus on, or use all that are included in the results by default.
export function deriveAggregatePartyVotesByType(
  votingData: any,
  voteTypes?: string[] | string
): { demVotes: number; gopVotes: number } {
  // instnatiate party & total new votes in this voting data block
  let newVotesDem: number = 0;
  let newVotesGop: number = 0;

  // given an optional param, derive a list ov vote types to iterate over
  const iterateVoteTypes: string[] = (voteTypes instanceof String
    ? [voteTypes]
    : voteTypes || Object.keys(votingData)) as string[];

  // iterate over the applicable types and create and add to sum values
  for (let voteType of iterateVoteTypes) {
    const { new_bidenj = 0, new_trumpd = 0 }: any = votingData[voteType] || {};

    newVotesDem += new_bidenj;
    newVotesGop += new_trumpd;
  }

  // return the difference between the two
  return { demVotes: newVotesDem, gopVotes: newVotesGop };
}


// Shape of the response object when finding voting data at a timestamp of interest.
export interface VotingDataAtTimestamp {
  index: number;
  votesForBiden: number;
  votesForTrump: number;
  totalVotes: number;
}

export function findVotingDataAtTimestamp(
  timestamp: string,
  timeseriesData: any[]
): VotingDataAtTimestamp {
  // reference to the sum total votes at a given timestamp from our
  // state-level timeseries voter data
  let votingDataAtTimestamp: any = {};

  let index: number = -1;

  // iterate through the timeseries data to extract the info at this timestamp.
  for (let currentIndex in timeseriesData) {
    const votingData = timeseriesData[currentIndex];

    // compare the timestamps to determine a match
    if (votingData.timestamp === timestamp) {
      // assign the reference of the current data tot his
      votingDataAtTimestamp = votingData;

      // remove the timeseries entry for shorter future iterations
      index = parseInt(currentIndex);

      // stop the loop
      break;
    }
  }

  // destrucutre vote tallies from the data at this timestamp
  const { biden = 0, trump = 0 } = votingDataAtTimestamp;

  return {
    index,
    votesForBiden: biden,
    votesForTrump: trump,
    totalVotes: biden + trump,
  };
}


// Given a list of Voting Data, iterate through the list and
// sum up each vote tally by vote type. Return an object
// with keys reflecting each type
export function sumVotingTotalsByType(votingData: any[]): any {
  const totals: any = {};

  votingData?.forEach(function (votingData: any) {
    const { results } = votingData;

    Object.keys(results).forEach(function (voteType: string) {
      const { new_bidenj, new_trumpd }: any = results[voteType];

      totals[voteType] = totals[voteType] || {};

      const bidenTally: number = totals[voteType]["biden"] || 0;
      const trumpTally: number = totals[voteType]["trump"] || 0;

      totals[voteType]["biden"] = bidenTally + new_bidenj;
      totals[voteType]["trump"] = trumpTally + new_trumpd;
    });
  });

  return totals;
}

// Helper function to loop through the voting totals and sum all votes
// previously filtered by type.
export function tallyAllVotesForCandidate(
  candidate: string,
  votingTotals: any[]
) {
  let allVotes: number = 0;

  for (let voteType in votingTotals) {
    const candidateVotes: number = votingTotals[voteType][candidate];

    allVotes += candidateVotes;
  }

  return allVotes;
}
