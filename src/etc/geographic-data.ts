export interface StateDetails {
  name: string;
  abbr: string;
  centerCoords: [number, number];
  timezone: string;
}

export type StateInformation = { [key: string]: StateDetails };

export const geographicConstants: StateInformation = {
  GA: {
    name: "Georgia",
    abbr: "GA",
    centerCoords: [33.247875, -83.441162],
    timezone: "America/New_York",
  },

  MI: {
    name: "Michigan",
    abbr: "MI",
    centerCoords: [44.182205, -84.506836],
    timezone: "America/Detroit",
  },

  NC: {
    name: "North Carolina",
    abbr: "NC",
    centerCoords: [35.7596, -79.0193],
    timezone: "America/New_York",
  },

  PA: {
    name: "Pennsylvania",
    abbr: "PA",
    centerCoords: [41.203323, -77.194527],
    timezone: "America/New_York",
  },
};

// given a precinct ID (as 'geo_id') and a list of available precincts, 
// iterate through the list and find/return the item in the list which 
// atches the supplied ID value.
export function findPrecinctVotingDataById(
  precinctId: string | undefined,
  precinctList: any[] | undefined
): any | undefined {
  if (precinctList && precinctId) {
    for (let precinctData of precinctList) {
      if (precinctData.geo_id === precinctId) {
        return precinctData;
      }
    }
  }
}
