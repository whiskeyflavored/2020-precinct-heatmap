const apiBaseUrl: string = `https://www.donutcovfefe.com`;

// Generic function which takes an endpoint url, and optional headers,
// and fetches the resource, and loads the entire response through a stream.
// This should be invoked by API functions rather than those functions trying
// to invoke `fetch` method within them.
export async function fetchStreamDataFromSource(
  fileUri: string,
  requestInfo: RequestInit = {
    headers: {
      Origin: "http://localhost:3000",
    },
  }
): Promise<string | undefined> {
  try {
    // load in time-series data locally into app
    const response: Response = await fetch(fileUri, requestInfo);

    // decode uint-8 bit stream from the stream response
    const decoder: TextDecoder = new TextDecoder();

    // body may be null
    if (response.body) {
      // set a reference to the reader to interpret the response
      const reader: ReadableStreamReader = response.body.getReader();

      // will hold incoming data as it completes its incoming stream
      let streamedData: string = "";

      // continue fetching streaming data until done
      while (true) {
        const { value, done } = await reader.read();
        if (done) break;
        streamedData = streamedData.concat(decoder.decode(value));
      }

      // capture into app/component state
      return streamedData;
    }
    // catch for errors
  } catch (error) {
    throw error;
  }
}

async function fetchResponseHandler(fetchResponse: string | undefined) {
  if (fetchResponse) {
    try {
      return JSON.parse(fetchResponse);
    } catch (error) {
      return { success: false, error };
    }
  } else {
    throw new Error(`no response was provided from API call`);
  }
}

export async function getAvailableCountiesInState(stateAbbr: string) {
  const endpoint: string = `county_data_in_state`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?state=${stateAbbr}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

// return a list of precincts in a state, with a list of unique county names
export async function getAvailablePrecinctsForState(
  stateAbbr: string
): Promise<any> {
  const endpoint: string = `county_precincts_in_state`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?state=${stateAbbr}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

export async function getAvailableStates() {
  const endpoint: string = `state_list`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

export async function getCountyGeographyDataForState(stateAbbr: string) {
  const endpoint: string = `state_geo_county`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?state=${stateAbbr}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

// Given a state abbreviation, grab the relevant precinct-level GeoJson for that state
export async function getPrecinctGeographyDataForState(
  stateAbbr: string
): Promise<any> {
  const endpoint: string = `state_geo`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?state=${stateAbbr}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

// returns aggregated voter data state-wude based on a timestamp
export async function getVoterDataForStateAtTimestamp(
  timestamp: string,
  stateAbbr: string
): Promise<any> {
  const endpoint: string = `totals_at_timestamp_in_state`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?state=${stateAbbr}&timestamp=${timestamp}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

export async function getVoteTotalsForPrecinct(
  stateAbbr: string,
  countyName: string,
  precinctName: string
): Promise<any> {
  const endpoint: string = `precinct_timestamp_totals`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?state=${stateAbbr}&county=${countyName}&precinct=${precinctName}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

export async function getVoteTotalsForState(stateAbbr: string): Promise<any> {
  const endpoint: string = `state_timestamp_totals`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?state=${stateAbbr}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

// Given a state abbreviation, get a list of all timestamps for that state.
// This aggregates all of the available timestamps in the voting data time series data.
// Note: current timeseries source is NYT-Edison published data.
export async function getTimestampsForState(stateAbbr: string): Promise<any> {
  const endpoint: string = `timestamps_for_state`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?state=${stateAbbr}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

// Given a precinct within a particular state, get a list of all timestamps for which
// data exists which applies to the precinct in the query.
export async function getTimeStampsForPrecintInState(
  stateAbbr: string,
  precinctName: string
): Promise<any> {
  const endpoint: string = `timestamps_for_district`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?precinct=${precinctName}&state=${stateAbbr}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

export async function getVoteTypesForState(
  stateAbbr: string
): Promise<string[]> {
  const endpoint = `vote_type_for_state`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?state=${stateAbbr}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

export async function getVotingDataAtTimestampForState(
  timestamp: string,
  stateAbbr: string
): Promise<any> {
  const endpoint: string = `data_for_timestamp_for_state_with_percent_delta`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?timestamp=${timestamp}&state=${stateAbbr}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

export async function getVotingDataAtTimestampForStateByType(
  stateAbbr: string,
  timestamp: string
): Promise<any> {
  const endpoint: string = `state_precinct_timestamp_votetypes`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?timestamp=${timestamp}&state=${stateAbbr}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}

// return a list of precincts in a state, with a list of unique county names
export async function searchForPrecintsInState(
  stateAbbr: string,
  query: string
): Promise<any> {
  const endpoint: string = `state_precincts`;

  const apiUrl: string = `${apiBaseUrl}/${endpoint}?state=${stateAbbr}&name=${query}`;

  return fetchStreamDataFromSource(apiUrl).then(fetchResponseHandler);
}
