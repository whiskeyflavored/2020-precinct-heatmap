import { easing } from "ts-easing";
import { timelineSizes } from "../etc/constants";

// Given a specific selected marker, animate the timeline viewbox
// to neatly center the marker target. Requires a max width of the
// timeline to be supplied (calculated when new timeline data is fetched)
export function centerViewboxOnTimestampMarker(
  position: number,
  svgElement: SVGSVGElement,
  maxWidth: number,
  options: { transition?: boolean; transitionDuration?: number }
) {
  return new Promise<void>(function (resolve, reject) {
    const { overscrollThreshold } = timelineSizes;

    const { transition, transitionDuration = 600 } = options || {};

    const svgBounds: ClientRect = svgElement.getBoundingClientRect();

    const bounds: any = {
      min: -overscrollThreshold,
      max: maxWidth + overscrollThreshold - svgBounds.width,
    };

    const viewboxOffsetX: number = svgElement.viewBox.baseVal?.x || 0;

    const centeringOffsetX: number = position - svgBounds.width / 2;

    const newViewboxOffsetX: number =
      position < svgBounds.width / 2 - overscrollThreshold
        ? bounds.min
        : centeringOffsetX > bounds.max
        ? bounds.max
        : centeringOffsetX;

    if (transition) {
      transitionCounter({
        duration: transitionDuration,
        method: assignViewboxOffsetAtT,
      }).then(function () {
        resolve();
      });
    } else {
      assignViewboxOffsetAtT(1);
      resolve();
    }

    function assignViewboxOffsetAtT(t: number): boolean {
      try {
        const easeT: number = easing.inOutQuad(t);

        const currentOffsetX: number =
          -(viewboxOffsetX - newViewboxOffsetX) * easeT + viewboxOffsetX;

        svgElement.setAttribute(
          "viewBox",
          `${currentOffsetX} 0 ${svgBounds.width} ${svgBounds.height}`
        );

        return true;
      } catch (error) {
        console.warn(new Error(`could not ease transition in timeline`), error);
        return false;
      }
    }
  });
}

// callback to pan the timeline position given a supplied delta.
// mostly handles the viewbox attribute resolution, bound by an overscroll threshold.
export function panTimeline(
  panX: number,
  timelineWidth: number,
  reactRef: React.RefObject<SVGSVGElement>
) {
  if (reactRef.current) {
    // reference to the react element's bounding rect to grab the current rendered dimensions
    const bounds: ClientRect = reactRef.current.getBoundingClientRect();

    // reference to a constant value which allows some overscroll amount to be renddered
    // within the timeline viewbox
    const { overscrollThreshold } = timelineSizes;

    const minPanX: number = -overscrollThreshold;

    const maxPanX: number = timelineWidth + overscrollThreshold - bounds.width;

    const boundedPanX: number =
      panX < minPanX ? minPanX : panX > maxPanX ? maxPanX : panX;

    // check if the bounded pan value falls within the max and min values before settting the attribute
    if (boundedPanX >= minPanX && boundedPanX <= maxPanX) {
      // set the viewbox attribute of the SVG ref element based on calculated offsets.
      reactRef.current.setAttribute(
        "viewBox",
        `${boundedPanX} 0 ${bounds.width} ${bounds.height}`
      );
    }
  }
}

/*
Generic function which will trigger a supplied method at every
available browser frame for a supplied duration (in Milliseconds) of time.
*/
function transitionCounter({ duration, method }: any) {
  // get the current time reference in milliseconds
  const startTime: number = Date.now();

  const nextTick = function (
    resolve: (value: unknown) => void,
    reject: (reason: any) => void
  ) {
    // get the current time at this tick in milliseconds
    const currentTime: number = Date.now();

    // determine how much time has passed since the startTime
    const lapsedTime: number = currentTime - startTime;

    // if this time difference is less than the duration, recurseively update
    if (lapsedTime < duration) {
      // find the coefficient from 0 < t < 1
      const t: number = lapsedTime / duration;

      // trigger the next frame request with a handler.
      // if the handler returns true, continue with the next tick
      const reqFrame = requestAnimationFrame(() => {
        const methodResult: boolean = method(t, reqFrame);

        if (methodResult) {
          return nextTick(resolve, reject);
        }
      });
    } else {
      resolve(true);
    }
  };

  return new Promise(nextTick);
}
