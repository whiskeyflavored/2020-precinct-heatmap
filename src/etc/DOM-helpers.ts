export function matchParent(target: Element, matchingFn: any): HTMLElement | undefined {
  if (target.parentNode) {
    return target.parentNode && matchingFn(target)
      ? (target as HTMLButtonElement)
      : matchParent(target.parentNode as Element, matchingFn);
  } else {
    return undefined;
  }
}
